/****************************************************************************
**  settings.cpp
**
**  Copyright information
**
**      Copyright (C) 2013-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "settings.h"

QString  Settings::filename  = "<username>@<server>";
Settings *Settings::instance = NULL;

/**
 * Constructor.
 *
 * @param *parent Pointer to parent.
 */
Settings::Settings (QObject *parent) : QObject(parent)
{
    instance          = this;
    misc              = Misc::getInstance();
    userSettingsFile  = NULL;
    loginSettingsFile = NULL;

    readLoginSettings();
}

/**
 * Returns integer setting.
 *
 * @param type    Setting type.
 * @param setting Setting name.
 * @return Returns setting value.
 */
int Settings::getInt(QString type, QString setting)
{
    return settings.value(type).value(setting).toInt();
}

/**
 * Returns string setting.
 *
 * @param type    Setting type.
 * @param setting Setting name.
 * @return Returns setting value.
 */
QString Settings::getString(QString type, QString setting)
{
    return settings.value(type).value(setting).toString();
}

/**
 * Returns font setting.
 *
 * @param type    Setting type.
 * @param setting Setting name.
 * @return Returns setting value.
 */
QFont Settings::getFont(QString type, QString setting)
{
    QFont font;

    font.fromString(settings.value(type).value(setting).toString());

    return font;
}

/**
 * Returns boolean setting.
 *
 * @param type    Setting type.
 * @param setting Setting name.
 * @return Returns setting value.
 */
bool Settings::getBool(QString type, QString setting)
{
    return settings.value(type).value(setting).toBool();
}

/**
 * Sets integer setting.
 *
 * @param type    Setting type.
 * @param setting Setting name.
 * @param value   Setting value.
 */
void Settings::setInt(QString type, QString setting, int value)
{
    QVariantMap settingType = settings.value(type);

    settingType.insert(setting, value);

    settings.insert(type, settingType);

    Q_EMIT intSettingChanged(type, setting, value);
}

/**
 * Sets string setting.
 *
 * @param type    Setting type.
 * @param setting Setting name.
 * @param value   Setting value.
 */
void Settings::setString(QString type, QString setting, QString value)
{
    QVariantMap settingType = settings.value(type);

    settingType.insert(setting, value);

    settings.insert(type, settingType);

    Q_EMIT stringSettingChanged(type, setting, value);
}

/**
 * Sets font setting.
 *
 * @param type    Setting type.
 * @param setting Setting name.
 * @param value   Setting value.
 */
void Settings::setFont(QString type, QString setting, QFont value)
{
    QVariantMap settingType = settings.value(type);

    settingType.insert(setting, value.toString());

    settings.insert(type, settingType);

    Q_EMIT fontSettingChanged(type, setting, value);
}

/**
 * Sets bool setting.
 *
 * @param type    Setting type.
 * @param setting Setting name.
 * @param value   Setting value.
 */
void Settings::setBool(QString type, QString setting, bool value)
{
    QVariantMap settingType = settings.value(type);

    settingType.insert(setting, value);

    settings.insert(type, settingType);

    Q_EMIT boolSettingChanged(type, setting, value);
}

/**
 * writes login settings to file.
 */
void Settings::writeLoginSettings ()
{
    if ( loginSettingsFile )
    {
        QVariantMap loginSettings = settings.value("Login");

        loginSettingsFile->setValue("Login/Server", loginSettings.value("Server"));
        loginSettingsFile->setValue("Login/Username", loginSettings.value("Username"));
        loginSettingsFile->setValue("Login/Password", loginSettings.value("Password"));
        loginSettingsFile->setValue("Login/UseSsl", loginSettings.value("UseSsl"));
        loginSettingsFile->setValue("Login/IgnoreSslErrors", loginSettings.value("IgnoreSslErrors"));
        loginSettingsFile->setValue("Login/RememberMe", loginSettings.value("RememberMe"));
        loginSettingsFile->setValue("Login/RememberPassword", loginSettings.value("RememberPassword"));
        loginSettingsFile->setValue("Login/LoginOnStartup", loginSettings.value("LoginOnStartup"));
        loginSettingsFile->setValue("Login/Reconnect", loginSettings.value("Reconnect"));
    }
}

/**
 * writes user settings to file.
 */
void Settings::writeUserSettings ()
{
    if ( userSettingsFile )
    {
        QVariantMap systraySettings = settings.value("Systray");

        userSettingsFile->setValue("Systray/ShowSystrayIcon", systraySettings.value("ShowSystrayIcon"));
        userSettingsFile->setValue("Systray/MinimizeToSystray", systraySettings.value("MinimizeToSystray"));
        userSettingsFile->setValue("Systray/CloseToSystray", systraySettings.value("CloseToSystray"));

        QVariantMap alertSettings = settings.value("Alert");

        userSettingsFile->setValue("Alert/WindowClosed", alertSettings.value("WindowClosed"));
        userSettingsFile->setValue("Alert/WindowDeactive", alertSettings.value("WindowDeactive"));
        userSettingsFile->setValue("Alert/WindowActive", alertSettings.value("WindowActive"));
        userSettingsFile->setValue("Alert/ShowNotification", alertSettings.value("ShowNotification"));
        userSettingsFile->setValue("Alert/BlinkSystray", alertSettings.value("BlinkSystray"));

        QVariantMap selectingSettings = settings.value("Selecting");

        userSettingsFile->setValue("Selecting/SelectTimestamp", selectingSettings.value("SelectTimestamp"));

        QVariantMap timestampSettings = settings.value("Timestamp");

        userSettingsFile->setValue("Timestamp/Enabled", timestampSettings.value("Enabled"));
        userSettingsFile->setValue("Timestamp/Format", timestampSettings.value("Format"));

        QVariantMap prioritySettings = settings.value("Priority");

        userSettingsFile->setValue("Priority/Priority", prioritySettings.value("Priority"));

        QVariantMap scrollbackSettings = settings.value("Scrollback");

        userSettingsFile->setValue("Scrollback/Scrollback", scrollbackSettings.value("Scrollback"));

        QVariantMap colorSettings = settings.value("Colors");

        userSettingsFile->setValue("Colors/Foreground", colorSettings.value("Foreground"));
        userSettingsFile->setValue("Colors/Background", colorSettings.value("Background"));
        userSettingsFile->setValue("Colors/Local0", colorSettings.value("Local0"));
        userSettingsFile->setValue("Colors/Local1", colorSettings.value("Local1"));
        userSettingsFile->setValue("Colors/Local2", colorSettings.value("Local2"));
        userSettingsFile->setValue("Colors/Local3", colorSettings.value("Local3"));
        userSettingsFile->setValue("Colors/Local4", colorSettings.value("Local4"));
        userSettingsFile->setValue("Colors/Local5", colorSettings.value("Local5"));
        userSettingsFile->setValue("Colors/Local6", colorSettings.value("Local6"));
        userSettingsFile->setValue("Colors/Local7", colorSettings.value("Local7"));
        userSettingsFile->setValue("Colors/Local8", colorSettings.value("Local8"));
        userSettingsFile->setValue("Colors/Local9", colorSettings.value("Local9"));
        userSettingsFile->setValue("Colors/Local10", colorSettings.value("Local10"));
        userSettingsFile->setValue("Colors/Local11", colorSettings.value("Local11"));
        userSettingsFile->setValue("Colors/Local12", colorSettings.value("Local12"));
        userSettingsFile->setValue("Colors/Local13", colorSettings.value("Local13"));
        userSettingsFile->setValue("Colors/Local14", colorSettings.value("Local14"));
        userSettingsFile->setValue("Colors/Local15", colorSettings.value("Local15"));
        userSettingsFile->setValue("Colors/Mirc0", colorSettings.value("Mirc0"));
        userSettingsFile->setValue("Colors/Mirc1", colorSettings.value("Mirc1"));
        userSettingsFile->setValue("Colors/Mirc2", colorSettings.value("Mirc2"));
        userSettingsFile->setValue("Colors/Mirc3", colorSettings.value("Mirc3"));
        userSettingsFile->setValue("Colors/Mirc4", colorSettings.value("Mirc4"));
        userSettingsFile->setValue("Colors/Mirc5", colorSettings.value("Mirc5"));
        userSettingsFile->setValue("Colors/Mirc6", colorSettings.value("Mirc6"));
        userSettingsFile->setValue("Colors/Mirc7", colorSettings.value("Mirc7"));
        userSettingsFile->setValue("Colors/Mirc8", colorSettings.value("Mirc8"));
        userSettingsFile->setValue("Colors/Mirc9", colorSettings.value("Mirc9"));
        userSettingsFile->setValue("Colors/Mirc10", colorSettings.value("Mirc10"));
        userSettingsFile->setValue("Colors/Mirc11", colorSettings.value("Mirc11"));
        userSettingsFile->setValue("Colors/Mirc12", colorSettings.value("Mirc12"));
        userSettingsFile->setValue("Colors/Mirc13", colorSettings.value("Mirc13"));
        userSettingsFile->setValue("Colors/Mirc14", colorSettings.value("Mirc14"));
        userSettingsFile->setValue("Colors/Mirc15", colorSettings.value("Mirc15"));
        userSettingsFile->setValue("Colors/ChatlistNewMessage", colorSettings.value("ChatlistNewMessage"));
        userSettingsFile->setValue("Colors/ChatlistHighlight", colorSettings.value("ChatlistHighlight"));
        userSettingsFile->setValue("Colors/ChatlistPrivate", colorSettings.value("ChatlistPrivate"));

        QVariantMap fontSettings = settings.value("Font");

        userSettingsFile->setValue("Font/ChatBox", fontSettings.value("ChatBox"));
    }
}

/**
 * Reads user settings from file.
 *
 * @param user   Username.
 * @param server Server.
 */
void Settings::readUserSettings (QString server, QString username, bool temporary)
{
    QVariantMap userSettings;

    if ( userSettingsFile )
    {
        delete userSettingsFile;
    }

    int version = 0;

    if ( !temporary )
    {
        QString file = filename.replace("<username>", username).replace("<server>", server);

        userSettingsFile = new QSettings("irggu", file);

        version = userSettingsFile->value("Version", 0).toInt();

        if ( version != 0 )
        {
            QStringListIterator settingsIt(userSettingsFile->allKeys());
            QString setting;

            while ( settingsIt.hasNext() )
            {
                setting = settingsIt.next();

                userSettings.insert(setting, userSettingsFile->value(setting));
            }
        }
    }

    switch ( version )
    {
        case 0:
        {
            userSettings.insert("Version", 1);

            userSettings.insert("Systray/ShowSystrayIcon", true);
            userSettings.insert("Systray/MinimizeToSystray", false);
            userSettings.insert("Systray/CloseToSystray", true);

            userSettings.insert("Alert/WindowClosed", true);
            userSettings.insert("Alert/WindowDeactive", true);
            userSettings.insert("Alert/WindowActive", false);
            userSettings.insert("Alert/ShowNotification", true);
            userSettings.insert("Alert/BlinkSystray", true);

            userSettings.insert("Selecting/SelectTimestamp", false);

            userSettings.insert("Timestamp/Enabled", true);
            userSettings.insert("Timestamp/Format", "[dd.MM.yy hh:mm:ss]");

            userSettings.insert("Priority/Priority", 9);

            userSettings.insert("Scrollback/Scrollback", 500);

            userSettings.insert("Colors/Foreground", "#000000");
            userSettings.insert("Colors/Background", "#ffffff");
            userSettings.insert("Colors/Local0", "#CCCCCC");
            userSettings.insert("Colors/Local1", "#000000");
            userSettings.insert("Colors/Local2", "#3636B2");
            userSettings.insert("Colors/Local3", "#2A8C2A");
            userSettings.insert("Colors/Local4", "#C33B3B");
            userSettings.insert("Colors/Local5", "#C73232");
            userSettings.insert("Colors/Local6", "#80267F");
            userSettings.insert("Colors/Local7", "#66361F");
            userSettings.insert("Colors/Local8", "#4C4C4C");
            userSettings.insert("Colors/Local9", "#4545E6");
            userSettings.insert("Colors/Local10", "#1A5555");
            userSettings.insert("Colors/Local11", "#2F8C74");
            userSettings.insert("Colors/Local12", "#4545E6");
            userSettings.insert("Colors/Local13", "#B037B0");
            userSettings.insert("Colors/Local14", "#C73232");
            userSettings.insert("Colors/Local15", "#959595");
            userSettings.insert("Colors/Mirc0", "#CCCCCC");
            userSettings.insert("Colors/Mirc1", "#000000");
            userSettings.insert("Colors/Mirc2", "#3636B2");
            userSettings.insert("Colors/Mirc3", "#2A8C2A");
            userSettings.insert("Colors/Mirc4", "#C33B3B");
            userSettings.insert("Colors/Mirc5", "#C73232");
            userSettings.insert("Colors/Mirc6", "#80267F");
            userSettings.insert("Colors/Mirc7", "#66361F");
            userSettings.insert("Colors/Mirc8", "#D9A641");
            userSettings.insert("Colors/Mirc9", "#3DCC3D");
            userSettings.insert("Colors/Mirc10", "#1A5555");
            userSettings.insert("Colors/Mirc11", "#2F8C74");
            userSettings.insert("Colors/Mirc12", "#4545E6");
            userSettings.insert("Colors/Mirc13", "#B037B0");
            userSettings.insert("Colors/Mirc14", "#4C4C4C");
            userSettings.insert("Colors/Mirc15", "#959595");
            userSettings.insert("Colors/ChatlistNewMessage", "#C73232");
            userSettings.insert("Colors/ChatlistHighlight", "#3636B2");
            userSettings.insert("Colors/ChatlistPrivate", "#3636B2");

            userSettings.insert("Font/ChatBox", "Monospace,9,-1,5,50,0,0,0,0,0");
        }
    }

    if ( !temporary )
    {
        QMapIterator<QString, QVariant> userSettingsIt(userSettings);

        while ( userSettingsIt.hasNext() )
        {
            userSettingsIt.next();

            userSettingsFile->setValue(userSettingsIt.key(), userSettingsIt.value());
        }
    }

    QVariantMap systraySettings;

    systraySettings.insert("ShowSystrayIcon", userSettings.value("Systray/ShowSystrayIcon", true));
    systraySettings.insert("MinimizeToSystray", userSettings.value("Systray/MinimizeToSystray", false));
    systraySettings.insert("CloseToSystray", userSettings.value("Systray/CloseToSystray", true));

    settings.insert("Systray", systraySettings);

    QVariantMap alertSettings;

    alertSettings.insert("WindowClosed", userSettings.value("Alert/WindowClosed", true));
    alertSettings.insert("WindowDeactive", userSettings.value("Alert/WindowDeactive", true));
    alertSettings.insert("WindowActive", userSettings.value("Alert/WindowActive", false));
    alertSettings.insert("ShowNotification", userSettings.value("Alert/ShowNotification", true));
    alertSettings.insert("BlinkSystray", userSettings.value("Alert/BlinkSystray", true));

    settings.insert("Alert", alertSettings);

    QVariantMap selectingSettings;

    selectingSettings.insert("SelectTimestamp", userSettings.value("Selecting/SelectTimestamp", false));

    settings.insert("Selecting", selectingSettings);

    QVariantMap timestampSettings;

    timestampSettings.insert("Enabled", userSettings.value("Timestamp/Enabled", true));
    timestampSettings.insert("Format", userSettings.value("Timestamp/Format", "[dd.MM.yy hh:mm:ss]"));

    settings.insert("Timestamp", timestampSettings);

    QVariantMap prioritySettings;

    prioritySettings.insert("Priority", userSettings.value("Priority/Priority", 9));

    settings.insert("Priority", prioritySettings);

    QVariantMap scrollbackSettings;

    scrollbackSettings.insert("Scrollback", userSettings.value("Scrollback/Scrollback", 500));

    settings.insert("Scrollback", scrollbackSettings);

    QVariantMap colorSettings;

    colorSettings.insert("Foreground", userSettings.value("Colors/Foreground", "#000000"));
    colorSettings.insert("Background", userSettings.value("Colors/Background", "#ffffff"));
    colorSettings.insert("Local0", userSettings.value("Colors/Local0", "#CCCCCC"));
    colorSettings.insert("Local1", userSettings.value("Colors/Local1", "#000000"));
    colorSettings.insert("Local2", userSettings.value("Colors/Local2", "#3636B2"));
    colorSettings.insert("Local3", userSettings.value("Colors/Local3", "#2A8C2A"));
    colorSettings.insert("Local4", userSettings.value("Colors/Local4", "#C33B3B"));
    colorSettings.insert("Local5", userSettings.value("Colors/Local5", "#C73232"));
    colorSettings.insert("Local6", userSettings.value("Colors/Local6", "#80267F"));
    colorSettings.insert("Local7", userSettings.value("Colors/Local7", "#66361F"));
    colorSettings.insert("Local8", userSettings.value("Colors/Local8", "#4C4C4C"));
    colorSettings.insert("Local9", userSettings.value("Colors/Local9", "#4545E6"));
    colorSettings.insert("Local10", userSettings.value("Colors/Local10", "#1A5555"));
    colorSettings.insert("Local11", userSettings.value("Colors/Local11", "#2F8C74"));
    colorSettings.insert("Local12", userSettings.value("Colors/Local12", "#4545E6"));
    colorSettings.insert("Local13", userSettings.value("Colors/Local13", "#B037B0"));
    colorSettings.insert("Local14", userSettings.value("Colors/Local14", "#C73232"));
    colorSettings.insert("Local15", userSettings.value("Colors/Local15", "#959595"));
    colorSettings.insert("Mirc0", userSettings.value("Colors/Mirc0", "#CCCCCC"));
    colorSettings.insert("Mirc1", userSettings.value("Colors/Mirc1", "#000000"));
    colorSettings.insert("Mirc2", userSettings.value("Colors/Mirc2", "#3636B2"));
    colorSettings.insert("Mirc3", userSettings.value("Colors/Mirc3", "#2A8C2A"));
    colorSettings.insert("Mirc4", userSettings.value("Colors/Mirc4", "#C33B3B"));
    colorSettings.insert("Mirc5", userSettings.value("Colors/Mirc5", "#C73232"));
    colorSettings.insert("Mirc6", userSettings.value("Colors/Mirc6", "#80267F"));
    colorSettings.insert("Mirc7", userSettings.value("Colors/Mirc7", "#66361F"));
    colorSettings.insert("Mirc8", userSettings.value("Colors/Mirc8", "#D9A641"));
    colorSettings.insert("Mirc9", userSettings.value("Colors/Mirc9", "#3DCC3D"));
    colorSettings.insert("Mirc10", userSettings.value("Colors/Mirc10", "#1A5555"));
    colorSettings.insert("Mirc11", userSettings.value("Colors/Mirc11", "#2F8C74"));
    colorSettings.insert("Mirc12", userSettings.value("Colors/Mirc12", "#4545E6"));
    colorSettings.insert("Mirc13", userSettings.value("Colors/Mirc13", "#B037B0"));
    colorSettings.insert("Mirc14", userSettings.value("Colors/Mirc14", "#4C4C4C"));
    colorSettings.insert("Mirc15", userSettings.value("Colors/Mirc15", "#959595"));
    colorSettings.insert("ChatlistNewMessage", userSettings.value("Colors/ChatlistNewMessage", "#C73232"));
    colorSettings.insert("ChatlistHighlight", userSettings.value("Colors/ChatlistHighlight", "#3636B2"));
    colorSettings.insert("ChatlistPrivate", userSettings.value("Colors/ChatlistPrivate", "#3636B2"));

    settings.insert("Colors", colorSettings);

    QVariantMap fontSettings;

    fontSettings.insert("ChatBox", userSettings.value("Font/ChatBox", "Monospace,9,-1,5,50,0,0,0,0,0"));

    settings.insert("Font", fontSettings);
}

/**
 * Gets instance.
 *
 * @return Instance.
 */
Settings *Settings::getInstance()
{
    return instance;
}

/**
 * Reads login settings from file.
 */
void Settings::readLoginSettings ()
{
    loginSettingsFile = new QSettings("irggu", "login");

    switch ( loginSettingsFile->value("Version", 0).toInt() )
    {
        case 0:
        {
            loginSettingsFile->setValue("Version", 1);
            loginSettingsFile->setValue("Login/Server", "");
            loginSettingsFile->setValue("Login/Username", "");
            loginSettingsFile->setValue("Login/Password", "");
            loginSettingsFile->setValue("Login/UseSsl", false);
            loginSettingsFile->setValue("Login/IgnoreSslErrors", false);
            loginSettingsFile->setValue("Login/RememberMe", false);
            loginSettingsFile->setValue("Login/RememberPassword", false);
            loginSettingsFile->setValue("Login/LoginOnStartup", false);
            loginSettingsFile->setValue("Login/Reconnect", 60);
        }
    }

    QVariantMap loginSettings;
    loginSettings.insert("RememberMe", loginSettingsFile->value("Login/RememberMe"));

    if ( loginSettings.value("RememberMe").toBool() )
    {
        loginSettings.insert("Server", loginSettingsFile->value("Login/Server"));
        loginSettings.insert("Username", loginSettingsFile->value("Login/Username"));
        loginSettings.insert("Password", loginSettingsFile->value("Login/Password"));
        loginSettings.insert("UseSsl", loginSettingsFile->value("Login/UseSsl"));
        loginSettings.insert("IgnoreSslErrors", loginSettingsFile->value("Login/IgnoreSslErrors"));
        loginSettings.insert("RememberMe", true);
        loginSettings.insert("RememberPassword", loginSettingsFile->value("Login/RememberPassword"));
        loginSettings.insert("LoginOnStartup", loginSettingsFile->value("Login/LoginOnStartup"));
        loginSettings.insert("Reconnect", loginSettingsFile->value("Login/Reconnect"));
    }

    settings.insert("Login", loginSettings);
}

/**
 * Signal is emitted when integer setting has changed.
 *
 * @fn void Settings::intSettingChanged(QString type, QString setting, int value)
 * @param type    Setting type.
 * @param setting Setting name.
 * @param value   Setting value.
 */

/**
 * Signal is emitted when string setting has changed.
 *
 * @fn void Settings::stringSettingChanged(QString type, QString setting, QString value)
 * @param type    Setting type.
 * @param setting Setting name.
 * @param value   Setting value.
 */

/**
 * Signal is emitted when font setting has changed.
 *
 * @fn void Settings::fontSettingChanged(QString type, QString setting, QFont value)
 * @param type    Setting type.
 * @param setting Setting name.
 * @param value   Setting value.
 */

/**
 * Signal is emitted when boolean setting has changed.
 *
 * @fn void Settings::boolSettingChanged(QString type, QString setting, bool value)
 * @param type    Setting type.
 * @param setting Setting name.
 * @param value   Setting value.
 */
