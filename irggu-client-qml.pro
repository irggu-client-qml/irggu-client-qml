#-------------------------------------------------
#
# Project created by QtCreator 2013-12-25T19:06:36
#
#-------------------------------------------------

QT += quick \
      qml \
      widgets \
      dbus

TARGET = irggu-client-qml
TEMPLATE = app

SOURCES += main.cpp \
    irgguclientqml.cpp \
    other/notify.cpp \
    other/systray.cpp \
    misc.cpp \
    settings.cpp \
    session.cpp \
    dbus/dbushandler.tpp

HEADERS  += \
    irgguclientqml.h \
    dbus/dbushandler.h \
    other/notify.h \
    other/systray.h \
    misc.h \
    settings.h \
    session.h

OTHER_FILES += \
    qml/dialogs/LoginDialog.qml \
    qml/widgets/NetworkListView.qml \
    qml/widgets/NameListView.qml \
    qml/widgets/ChatBoxView.qml \
    qml/widgets/ChatBoxListView.qml \
    qml/js/NetworkList.js \
    qml/js/IrGGu.js \
    qml/js/ChatBox.js \
    qml/js/Login.js \
    qml/js/NameList.js \
    qml/js/ChatBoxSelection.js \
    qml/dialogs/OptionsDialog.qml \
    qml/js/Options.js \
    qml/styles/ColorButtonStyle.qml \
    qml/MainWindow.qml \
    qml/dialogs/DccDialog.qml \
    qml/js/Dcc.js \
    qml/dialogs/ReconnectDialog.qml \
    qml/js/Reconnect.js

RESOURCES += \
    files.qrc

QMAKE_CXXFLAGS += -std=c++11

DISTFILES += \
    CMakeLists.txt

unix|win32: LIBS += -lirggu-client-lib
