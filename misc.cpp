/****************************************************************************
**  misc.cpp
**
**  Copyright information
**
**      Copyright (C) 2013-2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "misc.h"
#include "dbus/dbushandler.h"
#include <QDebug>
#include <QVariant>

Misc *Misc::instance = NULL;

/**
 * Constructor.
 *
 * @param *parent Pointer to parent.
 */
Misc::Misc (QObject *parent) : QObject(parent)
{
    instance      = this;
    notifyService = false;

    #ifdef Q_OS_UNIX

    QString service   = "org.freedesktop.DBus";
    QString path      = "/";
    QString interface = "org.freedesktop.DBus";
    QString method    = "GetNameOwner";

    QList<QVariant> arguments;
    arguments.append(QVariant(QString("org.freedesktop.Notifications")));

    DBusHandler<QString> dbusHandler;

    QList<QVariant> reply = dbusHandler.dbusReplyCall("session", service, path, interface,
                                                      method, arguments);

    if ( !reply.isEmpty() )
    {
        notifyService = true;
    }

    #endif
}

/**
 * Checks if there is notification D-Bus service available.
 *
 * @return True if notification service is available.
 */
bool Misc::hasNotifyService ()
{
    return notifyService;
}

/**
 * Gets instance.
 *
 * @return Instance.
 */
Misc *Misc::getInstance()
{
    return instance;
}
