/****************************************************************************
**  main.cpp
**
**  Copyright information
**
**      Copyright (C) 2013-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "irgguclientqml.h"
#include <QApplication>

/**
 * Main.
 *
 * @param argc  Argument count.
 * @param *argv Pointer to argument values.
 */
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setOrganizationName("IrGGu");
    a.setApplicationName("IrGGu-client");
    a.setWindowIcon(QIcon::fromTheme("irggu", QIcon(":/icons/irggu.svg")));

    IrGGuClientQml client(&a);

    return a.exec();
}
