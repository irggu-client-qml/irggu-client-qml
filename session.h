/****************************************************************************
**  session.h
**
**  Copyright information
**
**      Copyright (C) 2013-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef SESSION_H
#define SESSION_H

#include "settings.h"
#include <irggu-client/connection.h>
#include <QObject>
#include <QQuickWindow>
#include <QMap>
#include <QVariant>
#include <QDir>
#include <QFile>

typedef QList<QVariantMap>      DataList;
typedef QMap<QString, DataList> ChatData;
typedef QMap<QString, ChatData> ChatList;
typedef QMap<QString, ChatList> NetworkList;

/**
 *  This class is for handling session.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2015-02-10
 */
class Session : public QObject
{
    Q_OBJECT
public:
    explicit Session(QObject *parent = 0);

    void setGUI(QQuickWindow *gui);

    static Session *getInstance();

private:
    Settings     *settings;
    QQuickWindow *gui;
    QDir         *dataDir;
    QFile        *dataFile;
    NetworkList  networkList;
    int          sessionID;
    int          connectionID;
    int          scrollback;
    QString      sType;
    QString      sName;
    QString      server;
    QString      username;
    bool         temporary;

    static QString filename;
    static Session *instance;

    void openSession(QString server, QString username);
    void closeSession();
    void handleMsg(QString network, QString chat, Line line, DataList *chatLines, QString type);

Q_SIGNALS:
    void ready();
    void startLogin(QString server, QString username, QString password, bool useSsl,
                    bool ignoreSslErrors, int sessionID, int connectionID);
    void startLogout();
    void startSession(bool temporary);
    void reconnect();
    void writeLine(QString network, QString chat, QString line);
    void completeLine(QString network, QString chat, QString line, int pos);
    void writeSetting(QString category, QString setting, QString value);
    void writeDccFileReply(QString network, QString sender, QString file, bool get);
    void setIdle(bool idle, bool alerts, int priority);
    void quitNetwork(QString network);
    void closeChat(QString network, QString chat);
    void query(QString network, QString nick);
    void connected();
    void disconnected();
    void unableToConnect();
    void loginResult(QString result);
    void insertNetwork(QString network);
    void insertChat(QString network, QString chat);
    void insertNick(QString network, QString chat, QString nick, int index);
    void insertNicks(QString network, QString chat, QStringList nicks);
    void removeNetwork(QString network);
    void removeChat(QString network, QString chat);
    void removeNick(QString network, QString chat, int index);
    void renameChat(QString network, QString oldChat, QString newChat);
    void newMsg(QString network, QString chat, QString timestamp, QString nick, QString msg, QString type);
    void newDCCFile(QString network, QString sender, QString file);
    void closeDCCFile(QString network, QString sender, QString file);
    void lineCompleted(QString network, QString chat, QString line);
    void showAlert(QString network, QString sender, QString type);
    void closeAlert(QString network, QString sender, QString type);

public Q_SLOTS:
    void handleConnected();
    void handleDisconnected();
    void handleUnableToConnect();
    void handleLoginResult(int result, int sessionID, int connectionID);
    void handleInsertNetwork(QString network);
    void handleInsertChat(QString network, QString chat);
    void handleInsertNick(QString network, QString chat, QString nick);
    void handleInsertNicks(QString network, QString chat, QStringList nicks);
    void handleRemoveNetwork(QString network);
    void handleRemoveChat(QString network, QString chat);
    void handleRemoveNick(QString network, QString chat, QString nick);
    void handleChangeNick(QString network, QString chat, QString oldNick, QString newNick);
    void handleChangeNickMode(QString network, QString chat, QString nick, QString mode);
    void handleRenameChat(QString network, QString oldChat, QString newChat);
    void handleNewMsg(QString network, QString chat, Line lines);
    void handleNewHighlightMsg(QString network, QString chat, Line lines);
    void handleNewOwnMsg(QString network, QString chat, Line lines);
    void handleAppendMsg(QString network, QString chat, Line lines);
    void handleNewDCCFile(QString network, QString sender, QString file);
    void handleCloseDCCFile(QString network, QString sender, QString file);
    void handleLineCompleted(QString network, QString chat, QString line);
    void handleShowAlert(QString network, QString sender, int type);
    void handleCloseAlert(QString network, QString sender, int type);

private Q_SLOTS:
    void login(QString server, QString username, QString password, bool useSsl,
               bool ignoreSslErrors, bool temporary);
    void sendData(bool newOnly);
    void logout();
    void intSettingChanged(QString type, QString setting, int value);

};

#endif // SESSION_H
