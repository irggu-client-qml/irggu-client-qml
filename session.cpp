/****************************************************************************
**  session.cpp
**
**  Copyright information
**
**      Copyright (C) 2013-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "session.h"
#include <irggu-client/protocol/irggu.h>
#include <QMapIterator>
#include <QStandardPaths>
#include <QDebug>

QString Session::filename  = "<username>@<server>.dat";
Session *Session::instance = NULL;

/**
 * Constructor.
 *
 * @param *parent Pointer to parent.
 */
Session::Session (QObject *parent) : QObject(parent)
{
    instance = this;
    settings = Settings::getInstance();
    gui      = NULL;
    dataDir  = new QDir(QStandardPaths::writableLocation(QStandardPaths::DataLocation));
    dataFile = NULL;

    if ( !dataDir->exists() )
    {
        dataDir->mkpath(dataDir->path());
    }

    sessionID    = 0;
    connectionID = 0;
    scrollback   = 0;
    temporary    = false;
    sType        = "Scrollback";
    sName        = "Scrollback";

    connect(settings, SIGNAL(intSettingChanged(QString,QString,int)), this,
            SLOT(intSettingChanged(QString,QString,int)));
}

/**
 * Sets the GUI.
 *
 * @param gui GUI.
 */
void Session::setGUI (QQuickWindow *gui)
{
    this->gui = gui;

    QObject *loginDialog     = gui->findChild<QObject*>("loginDialog");
    QObject *optionsDialog   = gui->findChild<QObject*>("optionsDialog");
    QObject *dccDialog       = gui->findChild<QObject*>("dccDialog");
    QObject *reconnectDialog = gui->findChild<QObject*>("reconnectDialog");
    QObject *chatBox         = gui->findChild<QObject*>("chatBox");
    QObject *networkList     = gui->findChild<QObject*>("networkList");
    QObject *nameList        = gui->findChild<QObject*>("nameList");

    connect(loginDialog, SIGNAL(login(QString,QString,QString,bool,bool,bool)), this,
            SLOT(login(QString,QString,QString,bool,bool,bool)));
    connect(loginDialog, SIGNAL(logout()), this, SLOT(logout()));
    connect(optionsDialog, SIGNAL(writeSetting(QString,QString,QString)), this,
            SIGNAL(writeSetting(QString,QString,QString)));
    connect(dccDialog, SIGNAL(writeDccFileReply(QString,QString,QString,bool)), this,
            SIGNAL(writeDccFileReply(QString,QString,QString,bool)));
    connect(reconnectDialog, SIGNAL(reconnect()), this, SIGNAL(reconnect()));
    connect(gui, SIGNAL(readyForData(bool)), this, SLOT(sendData(bool)));
    connect(gui, SIGNAL(setIdle(bool,bool,int)), this, SIGNAL(setIdle(bool,bool,int)));
    connect(chatBox, SIGNAL(write(QString, QString, QString)), this,
            SIGNAL(writeLine(QString,QString,QString)));
    connect(chatBox, SIGNAL(complete(QString, QString, QString,int)), this,
            SIGNAL(completeLine(QString,QString,QString,int)));
    connect(networkList, SIGNAL(quitNetwork(QString)), this, SIGNAL(quitNetwork(QString)));
    connect(networkList, SIGNAL(closeChat(QString,QString)), this,
            SIGNAL(closeChat(QString,QString)));
    connect(nameList, SIGNAL(query(QString,QString)), this, SIGNAL(query(QString,QString)));

    Q_EMIT ready();
}

/**
 * Gets the instance.
 *
 * @return Instance.
 */
Session *Session::getInstance ()
{
    return instance;
}

/**
 * Opens session data file.
 *
 * @param server   Server.
 * @param username Username.
 */
void Session::openSession (QString server, QString username)
{
    this->server   = server.section('/', 0, 0);
    this->username = username;

    settings->readUserSettings(this->server, username, temporary);

    scrollback = settings->getInt(sType, sName);

    if ( !temporary )
    {
        QString file = filename.replace("<username>", username).replace("<server>", this->server);

        dataFile = new QFile(dataDir->filePath(file), this);

        if ( dataFile->exists() )
        {
            dataFile->open(QFile::ReadOnly);

            QDataStream in(dataFile);

            in >> networkList >> sessionID >> connectionID;

            dataFile->close();
        }
    }
}

/**
 * Closes session data file.
 */
void Session::closeSession ()
{
    if ( !temporary )
    {
        dataFile->open(QFile::WriteOnly);

        QDataStream out(dataFile);

        out << networkList << sessionID << connectionID;

        dataFile->close();

        dataFile->deleteLater();

        dataFile     = NULL;
        sessionID    = 0;
        connectionID = 0;
        temporary    = false;
    }
}

/**
 * Handles new messages and sends them to GUI.
 *
 * @param network   Network name.
 * @param chat      Chat name.
 * @param line      Message line.
 * @param chatLines Chat lines.
 * @param type      Message type.
 */
void Session::handleMsg (QString network, QString chat, Line line, DataList *chatLines,
                         QString type)
{
    QVariantMap chatLine;
    QString     timestamp;
    QString     nick;
    QString     msg;

    timestamp = line.value("timestamp");
    nick      = line.value("nick");
    msg       = line.value("msg");

    if ( chatLines )
    {
        chatLine.insert("timestamp", timestamp);
        chatLine.insert("nick", nick);
        chatLine.insert("msg", msg);
        chatLines->append(chatLine);

        if ( chatLines->size() > scrollback )
        {
            chatLines->removeFirst();
        }
    }

    Q_EMIT newMsg(network, chat, timestamp, nick, msg, type);
}

/**
 * Called when client has connected to server.
 */
void Session::handleConnected()
{
    Q_EMIT connected();
}

/**
 * Called when client has disconnected from server.
 */
void Session::handleDisconnected()
{
    Q_EMIT disconnected();
}

/**
 * Called when client was unable to connect to server.
 */
void Session::handleUnableToConnect()
{
    Q_EMIT unableToConnect();
}

/**
 * Called when client receives login result.
 *
 * @param result       Result.
 * @param sessionID    Session ID.
 * @param connectionID Connection ID.
 */
void Session::handleLoginResult (int result, int sessionID, int connectionID)
{
    if ( sessionID != this->sessionID || connectionID != this->connectionID )
    {
        networkList.clear();
    }

    this->sessionID    = sessionID;
    this->connectionID = connectionID;

    if ( dataFile )
    {
        dataFile->remove();
    }

    QString resultString;

    switch ( result )
    {
        case IrGGu_Login::noError:
        {
            resultString = "noError";

            break;
        }

        case IrGGu_Login::wrongUserPass:
        {
            resultString = "wrongUserPass";

            break;
        }

        case IrGGu_Login::maxClientLimit:
        {
            resultString = "maxClientLimit";

            break;
        }
    }

    Q_EMIT loginResult(resultString);
}

/**
 * Called when client receives insert network command.
 *
 * @param network Network name.
 */
void Session::handleInsertNetwork (QString network)
{
    DataList nameList;
    DataList lineList;
    ChatData chatData;
    ChatList chatList;

    nameList.insert(0, QVariantMap());
    chatData.insert("names", nameList);
    chatData.insert("lines", lineList);
    chatList.insert("(server)", chatData);
    networkList.insert(network, chatList);

    Q_EMIT insertNetwork(network);
}

/**
 * Called when client receives insert chat command.
 *
 * @param network Network name.
 * @param chat    Chat name.
 */
void Session::handleInsertChat (QString network, QString chat)
{
    if ( networkList.contains(network) )
    {
        ChatList chatList = networkList.value(network);

        DataList nameList;
        DataList lineList;
        ChatData chatData;

        nameList.insert(0, QVariantMap());
        chatData.insert("names", nameList);
        chatData.insert("lines", lineList);
        chatList.insert(chat, chatData);

        networkList.insert(network, chatList);

        Q_EMIT insertChat(network, chat);
    }
}

/**
 * Called when client receives insert nick command.
 *
 * @param network Network name.
 * @param chat    Chat name.
 * @param nick    Nick.
 */
void Session::handleInsertNick (QString network, QString chat, QString nick)
{
    if ( networkList.contains(network) )
    {
        ChatList chatList = networkList.value(network);

        if ( chatList.contains(chat) )
        {
            ChatData    chatData = chatList.value(chat);
            DataList    dataList = chatData.value("names");
            QVariantMap nameList = dataList.at(0);

            switch ( nick.at(0).unicode() )
            {
                case '@':
                {
                    QStringList names = nameList.value("@").toStringList();

                    names.append(nick);
                    names.sort(Qt::CaseInsensitive);

                    nameList.insert("@", names);

                    break;
                }

                case '%':
                {
                    QStringList names = nameList.value("%").toStringList();

                    names.append(nick);
                    names.sort(Qt::CaseInsensitive);

                    nameList.insert("%", names);

                    break;
                }

                case '+':
                {
                    QStringList names = nameList.value("+").toStringList();

                    names.append(nick);
                    names.sort(Qt::CaseInsensitive);

                    nameList.insert("+", names);

                    break;
                }

                default:
                {
                    QStringList names = nameList.value("0").toStringList();

                    names.append(nick);
                    names.sort(Qt::CaseInsensitive);

                    nameList.insert("0", names);

                    break;
                }
            }

            dataList.replace(0, nameList);
            chatData.insert("names", dataList);
            chatList.insert(chat, chatData);
            networkList.insert(network, chatList);

            QStringList allNames;

            allNames << nameList.value("@").toStringList();
            allNames << nameList.value("%").toStringList();
            allNames << nameList.value("+").toStringList();
            allNames << nameList.value("0").toStringList();

            Q_EMIT insertNick(network, chat, nick, allNames.indexOf(nick));
        }
    }
}

/**
 * Called when client receives insert nicks command.
 *
 * @param network Network name.
 * @param chat    Chat name.
 * @param nicks   Nicks.
 */
void Session::handleInsertNicks (QString network, QString chat, QStringList nicks)
{
    if ( networkList.contains(network) )
    {
        ChatList chatList = networkList.value(network);

        if ( chatList.contains(chat) )
        {
            ChatData            chatData = chatList.value(chat);
            DataList            dataList = chatData.value("names");
            QVariantMap         nameList = dataList.at(0);
            QStringListIterator nicksIt(nicks);
            QString             nick;

            while ( nicksIt.hasNext() )
            {
                nick = nicksIt.next();

                switch ( nick.at(0).unicode() )
                {
                    case '@':
                    {
                        QStringList names = nameList.value("@").toStringList();

                        names.append(nick);
                        names.sort(Qt::CaseInsensitive);

                        nameList.insert("@", names);

                        break;
                    }

                    case '%':
                    {
                        QStringList names = nameList.value("%").toStringList();

                        names.append(nick);
                        names.sort(Qt::CaseInsensitive);

                        nameList.insert("%", names);

                        break;
                    }

                    case '+':
                    {
                        QStringList names = nameList.value("+").toStringList();

                        names.append(nick);
                        names.sort(Qt::CaseInsensitive);

                        nameList.insert("+", names);

                        break;
                    }

                    default:
                    {
                        QStringList names = nameList.value("0").toStringList();

                        names.append(nick);
                        names.sort(Qt::CaseInsensitive);

                        nameList.insert("0", names);

                        break;
                    }
                }
            }

            dataList.replace(0, nameList);
            chatData.insert("names", dataList);
            chatList.insert(chat, chatData);
            networkList.insert(network, chatList);

            QStringList allNames;

            allNames << nameList.value("@").toStringList();
            allNames << nameList.value("%").toStringList();
            allNames << nameList.value("+").toStringList();
            allNames << nameList.value("0").toStringList();

            Q_EMIT insertNicks(network, chat, allNames);
        }
    }
}

/**
 * Called when client receives remove network command.
 *
 * @param network Network name.
 */
void Session::handleRemoveNetwork (QString network)
{
    if ( networkList.contains(network) )
    {
        networkList.remove(network);

        Q_EMIT removeNetwork(network);
    }
}

/**
 * Called when client receives remove chat command.
 *
 * @param network Network name.
 * @param chat    Chat name.
 */
void Session::handleRemoveChat (QString network, QString chat)
{
    if ( networkList.contains(network) )
    {
        ChatList chatList = networkList.value(network);

        if ( chatList.contains(chat) )
        {
            chatList.remove(chat);

            networkList.insert(network, chatList);

            Q_EMIT removeChat(network, chat);
        }
    }
}

/**
 * Called when client receives remove nick command.
 *
 * @param network Network name.
 * @param chat    Chat name.
 * @param nick    Nick.
 */
void Session::handleRemoveNick (QString network, QString chat, QString nick)
{
    if ( networkList.contains(network) )
    {
        ChatList chatList = networkList.value(network);

        if ( chatList.contains(chat) )
        {
            ChatData    chatData = chatList.value(chat);
            DataList    dataList = chatData.value("names");
            QVariantMap nameList = dataList.at(0);
            QStringList allNames;

            allNames << nameList.value("@").toStringList();
            allNames << nameList.value("%").toStringList();
            allNames << nameList.value("+").toStringList();
            allNames << nameList.value("0").toStringList();

            int index = allNames.indexOf(nick);

            switch ( nick.at(0).unicode() )
            {
                case '@':
                {
                    QStringList names = nameList.value("@").toStringList();

                    names.removeOne(nick);

                    nameList.insert("@", names);

                    break;
                }

                case '%':
                {
                    QStringList names = nameList.value("%").toStringList();

                    names.removeOne(nick);

                    nameList.insert("%", names);

                    break;
                }

                case '+':
                {
                    QStringList names = nameList.value("+").toStringList();

                    names.removeOne(nick);

                    nameList.insert("+", names);

                    break;
                }

                default:
                {
                    QStringList names = nameList.value("0").toStringList();

                    names.removeOne(nick);

                    nameList.insert("0", names);

                    break;
                }
            }

            dataList.replace(0, nameList);
            chatData.insert("names", dataList);
            chatList.insert(chat, chatData);
            networkList.insert(network, chatList);

            Q_EMIT removeNick(network, chat, index);
        }
    }
}

/**
 * Called when client receives change nick command.
 *
 * @param network Network name.
 * @param chat    Chat name.
 * @param oldNick Old nick.
 * @param newNick New nick.
 */
void Session::handleChangeNick (QString network, QString chat, QString oldNick, QString newNick)
{
    if ( networkList.contains(network) )
    {
        ChatList chatList = networkList.value(network);

        if ( chatList.contains(chat) )
        {
            ChatData    chatData = chatList.value(chat);
            DataList    dataList = chatData.value("names");
            QVariantMap nameList = dataList.at(0);
            QStringList allNames;

            allNames << nameList.value("@").toStringList();
            allNames << nameList.value("%").toStringList();
            allNames << nameList.value("+").toStringList();
            allNames << nameList.value("0").toStringList();

            int oldIndex = allNames.indexOf(oldNick);

            switch ( oldNick.at(0).unicode() )
            {
                case '@':
                {
                    QStringList names = nameList.value("@").toStringList();

                    names.removeOne(oldNick);
                    names.append(newNick);
                    names.sort(Qt::CaseInsensitive);

                    nameList.insert("@", names);

                    break;
                }

                case '%':
                {
                    QStringList names = nameList.value("%").toStringList();

                    names.removeOne(oldNick);
                    names.append(newNick);
                    names.sort(Qt::CaseInsensitive);

                    nameList.insert("%", names);

                    break;
                }

                case '+':
                {
                    QStringList names = nameList.value("+").toStringList();

                    names.removeOne(oldNick);
                    names.append(newNick);
                    names.sort(Qt::CaseInsensitive);

                    nameList.insert("+", names);

                    break;
                }

                default:
                {
                    QStringList names = nameList.value("0").toStringList();

                    names.removeOne(oldNick);
                    names.append(newNick);
                    names.sort(Qt::CaseInsensitive);

                    nameList.insert("0", names);

                    break;
                }
            }

            dataList.replace(0, nameList);
            chatData.insert("names", dataList);
            chatList.insert(chat, chatData);
            networkList.insert(network, chatList);

            allNames.clear();

            allNames << nameList.value("@").toStringList();
            allNames << nameList.value("%").toStringList();
            allNames << nameList.value("+").toStringList();
            allNames << nameList.value("0").toStringList();

            int newIndex = allNames.indexOf(newNick);

            Q_EMIT removeNick(network, chat, oldIndex);
            Q_EMIT insertNick(network, chat, newNick, newIndex);
        }
    }
}

/**
 * Called when client receives change nick mode command.
 *
 * @param network Network name.
 * @param chat    Chat name.
 * @param nick    Nick.
 * @param mode    New mode.
 */
void Session::handleChangeNickMode (QString network, QString chat, QString nick, QString mode)
{
    if ( networkList.contains(network) )
    {
        ChatList chatList = networkList.value(network);

        if ( chatList.contains(chat) )
        {
            ChatData    chatData = chatList.value(chat);
            DataList    dataList = chatData.value("names");
            QVariantMap nameList = dataList.at(0);
            QString     oldNick  = nick;
            QStringList allNames;

            allNames << nameList.value("@").toStringList();
            allNames << nameList.value("%").toStringList();
            allNames << nameList.value("+").toStringList();
            allNames << nameList.value("0").toStringList();

            int oldIndex = allNames.indexOf(oldNick);

            switch ( nick.at(0).unicode() )
            {
                case '@':
                {
                    QStringList names = nameList.value("@").toStringList();

                    names.removeOne(nick);

                    nameList.insert("@", names);

                    nick.remove(0, 1);

                    break;
                }

                case '%':
                {
                    QStringList names = nameList.value("%").toStringList();

                    names.removeOne(nick);

                    nameList.insert("%", names);

                    nick.remove(0, 1);

                    break;
                }

                case '+':
                {
                    QStringList names = nameList.value("+").toStringList();

                    names.removeOne(nick);

                    nameList.insert("+", names);

                    nick.remove(0, 1);

                    break;
                }

                default:
                {
                    QStringList names = nameList.value("0").toStringList();

                    names.removeOne(nick);

                    nameList.insert("0", names);

                    break;
                }
            }

            nick.prepend(mode);

            switch ( nick.at(0).unicode() )
            {
                case '@':
                {
                    QStringList names = nameList.value("@").toStringList();

                    names.append(nick);
                    names.sort(Qt::CaseInsensitive);

                    nameList.insert("@", names);

                    break;
                }

                case '%':
                {
                    QStringList names = nameList.value("%").toStringList();

                    names.append(nick);
                    names.sort(Qt::CaseInsensitive);

                    nameList.insert("%", names);

                    break;
                }

                case '+':
                {
                    QStringList names = nameList.value("+").toStringList();

                    names.append(nick);
                    names.sort(Qt::CaseInsensitive);

                    nameList.insert("+", names);

                    break;
                }

                default:
                {
                    QStringList names = nameList.value("0").toStringList();

                    names.append(nick);
                    names.sort(Qt::CaseInsensitive);

                    nameList.insert("0", names);

                    break;
                }
            }

            dataList.replace(0, nameList);
            chatData.insert("names", dataList);
            chatList.insert(chat, chatData);
            networkList.insert(network, chatList);

            allNames.clear();

            allNames << nameList.value("@").toStringList();
            allNames << nameList.value("%").toStringList();
            allNames << nameList.value("+").toStringList();
            allNames << nameList.value("0").toStringList();

            int newIndex = allNames.indexOf(nick);

            Q_EMIT removeNick(network, chat, oldIndex);
            Q_EMIT insertNick(network, chat, nick, newIndex);
        }
    }
}

/**
 * Called when client receives rename chat command.
 *
 * @param network Network name.
 * @param oldChat old Chat name.
 * @param newChat new Chat name.
 */
void Session::handleRenameChat (QString network, QString oldChat, QString newChat)
{
    if ( networkList.contains(network) )
    {
        ChatList chatList = networkList.value(network);

        if ( chatList.contains(oldChat) )
        {
            ChatData chatData = chatList.take(oldChat);

            chatList.insert(newChat, chatData);
            networkList.insert(network, chatList);

            Q_EMIT renameChat(network, oldChat, newChat);
        }
    }
}

/**
 * Called when client receives new message command.
 *
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Message line.
 */
void Session::handleNewMsg (QString network, QString chat, Line line)
{
    if ( networkList.contains(network) )
    {
        ChatList chatList = networkList.value(network);

        if ( chatList.contains(chat) )
        {
            ChatData chatData  = chatList.value(chat);
            DataList chatLines = chatData.value("lines");

            handleMsg(network, chat, line, &chatLines, "normal");

            chatData.insert("lines", chatLines);
            chatList.insert(chat, chatData);
            networkList.insert(network, chatList);
        } else if ( chat == "(current)" )
        {
            handleMsg(network, chat, line, NULL, "normal");
        }
    }
    else if ( network == "(current)" || network == "(none)" )
    {
        handleMsg(network, chat, line, NULL, "normal");
    }
}

/**
 * Called when client receives new highlight message command.
 *
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Message line.
 */
void Session::handleNewHighlightMsg (QString network, QString chat, Line line)
{
    if ( networkList.contains(network) )
    {
        ChatList chatList = networkList.value(network);

        if ( chatList.contains(chat) )
        {
            ChatData chatData  = chatList.value(chat);
            DataList chatLines = chatData.value("lines");

            handleMsg(network, chat, line, &chatLines, "highlight");

            chatData.insert("lines", chatLines);
            chatList.insert(chat, chatData);
            networkList.insert(network, chatList);
        }
    }
}

/**
 * Called when client receives new own message command.
 *
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Message line.
 */
void Session::handleNewOwnMsg (QString network, QString chat, Line line)
{
    if ( networkList.contains(network) )
    {
        ChatList chatList = networkList.value(network);

        if ( chatList.contains(chat) )
        {
            ChatData chatData  = chatList.value(chat);
            DataList chatLines = chatData.value("lines");

            handleMsg(network, chat, line, &chatLines, "own");

            chatData.insert("lines", chatLines);
            chatList.insert(chat, chatData);
            networkList.insert(network, chatList);
        }
    }
}

/**
 * Called when client receives append message command.
 *
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Message line.
 */
void Session::handleAppendMsg (QString network, QString chat, Line line)
{
    if ( networkList.contains(network) )
    {
        ChatList chatList = networkList.value(network);

        if ( chatList.contains(chat) )
        {
            ChatData chatData  = chatList.value(chat);
            DataList chatLines = chatData.value("lines");

            handleMsg(network, chat, line, &chatLines, "append");

            chatData.insert("lines", chatLines);
            chatList.insert(chat, chatData);
            networkList.insert(network, chatList);
        }
    }
}

/**
 * Called when client receives new DCC file command.
 *
 * @param network Network name.
 * @param sender  Sender's name.
 * @param file    File.
 */
void Session::handleNewDCCFile (QString network, QString sender, QString file)
{
    Q_EMIT newDCCFile(network, sender, file);
}

/**
 * Called when client receives close DCC file command.
 *
 * @param network Network name.
 * @param sender  Sender's name.
 * @param file    File.
 */
void Session::handleCloseDCCFile (QString network, QString sender, QString file)
{
    Q_EMIT closeDCCFile(network, sender, file);
}

/**
 * Called when client receives line completed command.
 *
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Completed line.
 */
void Session::handleLineCompleted (QString network, QString chat, QString line)
{
    Q_EMIT lineCompleted(network, chat, line);
}

/**
 * Called when client receives alert command.
 *
 * @param network Network name.
 * @param sender  Triggerer of alert.
 * @param type    Alert type.
 */
void Session::handleShowAlert (QString network, QString sender, int type)
{
    QString typeName = "";

    switch ( type )
    {
        case 0:
        {
            typeName = "private";

            break;
        }

        case 1:
        {
            typeName = "highlight";

            break;
        }

        case 2:
        {
            typeName = "dcc";

            break;
        }
    }

    Q_EMIT showAlert(network, sender, typeName);
}

/**
 * Called when client receives close alert command.
 *
 * @param network Network name.
 * @param sender  Triggerer of alert.
 * @param type    Alert type.
 */
void Session::handleCloseAlert (QString network, QString sender, int type)
{
    QString typeName = "";

    switch ( type )
    {
        case 0:
        {
            typeName = "private";

            break;
        }

        case 1:
        {
            typeName = "highlight";

            break;
        }

        case 2:
        {
            typeName = "dcc";

            break;
        }
    }

    Q_EMIT closeAlert(network, sender, typeName);
}

/**
 * Logs in.
 *
 * @param server          Server.
 * @param username        Username.
 * @param password        Password.
 * @param useSsl          Specifies if SSL is used.
 * @param ignoreSslErrors Specifies if SSL errors are ignored.
 * @param temporary       Specifies if session is temporary.
 */
void Session::login (QString server, QString username, QString password, bool useSsl,
                     bool ignoreSslErrors, bool temporary)
{
    this->temporary = temporary;

    openSession(server, username);

    Q_EMIT startLogin(server, username, password, useSsl, ignoreSslErrors, sessionID,
                      connectionID);
}

/**
 * Sends data of session data file to GUI.
 *
 * @param newOnly Only send new data.
 */
void Session::sendData (bool newOnly)
{
    if ( !newOnly )
    {
        QMapIterator<QString, ChatList> networkListIt(networkList);

        QString     network;
        QString     chat;
        ChatList    chatList;
        ChatData    chatData;
        DataList    lineList;
        QVariantMap nameList;
        QVariantMap line;

        while ( networkListIt.hasNext() )
        {
            networkListIt.next();

            network  = networkListIt.key();
            chatList = networkListIt.value();

            Q_EMIT insertNetwork(network);

            QMapIterator<QString, ChatData> chatListIt(chatList);

            while ( chatListIt.hasNext() )
            {
                chatListIt.next();

                chat     = chatListIt.key();
                chatData = chatListIt.value();
                lineList = chatData.value("lines");
                nameList = chatData.value("names").at(0);

                if ( chat != "(server)" )
                {
                    Q_EMIT insertChat(network, chat);
                }

                QListIterator<QVariantMap> lineListIt(lineList);

                while ( lineListIt.hasNext() )
                {
                    line = lineListIt.next();

                    Q_EMIT newMsg(network, chat, line.value("timestamp").toString(),
                                  line.value("nick").toString(), line.value("msg").toString(),
                                  "append");
                }

                QStringList allNames;

                allNames << nameList.value("@").toStringList();
                allNames << nameList.value("%").toStringList();
                allNames << nameList.value("+").toStringList();
                allNames << nameList.value("0").toStringList();

                Q_EMIT insertNicks(network, chat, allNames);
            }
        }
    }

    Q_EMIT startSession(temporary);
}

/**
 * Logs out.
 */
void Session::logout()
{
    closeSession();

    Q_EMIT startLogout();
}

/**
 * Called when integer setting has changed.
 *
 * @param type    Setting type.
 * @param setting Setting name.
 * @param value   Setting value.
 */
void Session::intSettingChanged(QString type, QString setting, int value)
{
    if ( type == sType && setting == sName )
    {
        scrollback = value;

        QMapIterator<QString, ChatList> networkListIt(networkList);

        QString  network;
        QString  chat;
        ChatList chatList;
        ChatData chatData;
        DataList chatLines;

        while ( networkListIt.hasNext() )
        {
            networkListIt.next();

            network  = networkListIt.key();
            chatList = networkListIt.value();

            QMapIterator<QString, ChatData> chatListIt(chatList);

            while ( chatListIt.hasNext() )
            {
                chatListIt.next();

                chat      = chatListIt.key();
                chatData  = chatListIt.value();
                chatLines = chatData.value("lines");

                while ( chatLines.size() > scrollback )
                {
                    chatLines.removeFirst();
                }

                chatData.insert("lines", chatLines);
                chatList.insert(chat, chatData);
                networkList.insert(network, chatList);
            }
        }
    }
}

/**
 * This signal is emitted when session is ready for login.
 *
 * @fn Session::ready()
 */

/**
 * This signal is emitted when GUI has initiated login.
 *
 * @fn Session::startLogin(QString server, QString username, QString password, bool useSsl,
 * bool ignoreSslErrors, int sessionID, int connectionID)
 * @param server          Server.
 * @param username        Username.
 * @param password        Password.
 * @param useSsl          Specifies if SSL is used.
 * @param ignoreSslErrors Specifies if SSL errors are ignored.
 * @param sessionID       Session ID of existing session.
 * @param connectionID    Connection ID of existing session.
 */

/**
 * This signal is emitted when GUI has initiated logout.
 *
 * @fn Session::startLogout
 */

/**
 * This signal is emitted after succesfully login.
 *
 * @fn Session::startSession(bool temporary)
 * @param temporary Specifies if session is temporary.
 */

/**
 * This signal is emitted when GUI has initiated reconnect.
 *
 * @fn Session::reconnect()
 */

/**
 * This signal is emitted when GUI has line to write.
 *
 * @fn Session::writeLine(QString network, QString chat, QString line)
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Line to be written.
 */

/**
 * This signal is emitted when GUI has line to complete.
 *
 * @fn Session::completeLine(QString network, QString chat, QString line, int pos)
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Line to be completed.
 * @param pos     Cursor position.
 */

/**
 * This signal is emitted when GUI has setting to write.
 *
 * @fn Session::writeSetting(QString category, QString setting, QString value)
 * @param category Setting category.
 * @param setting  Setting name.
 * @param value    Setting value.
 */

/**
 * This signal is emitted when GUI has DCC file reply to write.
 *
 * @fn Session::writeDccFileReply(QString network, QString sender, QString file, bool get)
 * @param network Network name.
 * @param sender  Sender's name.
 * @param file    Filename.
 * @param get     Specifies if the file should be accepted.
 */

/**
 * This signal is emitted when GUI has set idle command to write.
 *
 * @fn Session::setIdle(bool idle, bool alerts, int priority)
 * @param idle     Specifies if client is in idle state.
 * @param alerts   Specifies if client shows alerts.
 * @param priority Priority of client.
 */

/**
 * This signal is emitted when close network action has been selected.
 *
 * @fn Session::quitNetwork(QString network)
 * @param network Network name.
 */

/**
 * This signal is emitted when close chat action has been selected.
 *
 * @fn Session::closeChat(QString network, QString chat)
 * @param network Network name.
 * @param chat    Chat name.
 */

/**
 * This signal is emitted when query action has been selected.
 *
 * @fn Session::query(QString network, QString nick)
 * @param network Network name.
 * @param nick    Nick.
 */

/**
 * Signal is emitted when client has connected to server.
 *
 * @fn void Session::connected()
 */

/**
 * Signal is emitted when client has disconnected from server.
 *
 * @fn void Session::disconnected()
 */

/**
 * Signal is emitted when client was unable to connect to server.
 *
 * @fn void Session::unableToConnect()
 */

/**
 * Signal is emitted when login result has been received.
 *
 * @fn void Session::loginResult(QString result)
 * @param result Login Result.
 */

/**
 * Signal is emitted when insert network command has been received.
 *
 * @fn void Session::insertNetwork(QString network)
 * @param network Network name.
 */

/**
 * Signal is emitted when insert chat command has been received.
 *
 * @fn void Session::insertChat(QString network, QString chat)
 * @param network Network name.
 * @param chat    Chat name.
 */

/**
 * Signal is emitted when insert nick command has been received.
 *
 * @fn void Session::insertNick(QString network, QString chat, QString nick, int index)
 * @param network Network name.
 * @param chat    Chat name.
 * @param nick    Nick.
 * @param inde    Index of nick.
 */

/**
 * Signal is emitted when insert nicks command has been received.
 *
 * @fn void Session::insertNicks(QString network, QString chat, QStringList nicks)
 * @param network Network name.
 * @param chat    Chat name.
 * @param nicks   Nicks.
 */

/**
 * Signal is emitted when remove network command has been received.
 *
 * @fn void Session::removeNetwork(QString network)
 * @param network Network name.
 */

/**
 * Signal is emitted when remove chat command has been received.
 *
 * @fn void Session::removeChat(QString network, QString chat)
 * @param network Network name.
 * @param chat    Chat name.
 */

/**
 * Signal is emitted when remove nick command has been received.
 *
 * @fn void Session::removeNick(QString network, QString chat, int index)
 * @param network Network name.
 * @param chat    Chat name.
 * @param index   Index of nick.
 */

/**
 * Signal is emitted when rename chat command has been received.
 *
 * @fn void Session::renameChat(QString network, QString oldChat, QString newChat)
 * @param network Network name.
 * @param oldChat Old chat name.
 * @param newChat New chat name.
 */

/**
 * Signal is emitted when new message, new highlight message, new own message or append message
 * command has been received.
 *
 * @fn void Session::newMsg(QString network, QString chat, QString timestamp, QString nick,
 * QString msg, QString type)
 * @param network   Network name.
 * @param chat      Chat name.
 * @param timestamp Timestamp.
 * @param nick      Nick.
 * @param msg       Message.
 * @param type      Message type.
 */

/**
 * Signal is emitted when new DCC file command has been received.
 *
 * @fn void Session::newDCCFile(QString network, QString sender, QString file)
 * @param network Network name.
 * @param sender  Sender's name.
 * @param file    Filename.
 */

/**
 * Signal is emitted when close DCC file command has been received.
 *
 * @fn void Session::closeDCCFile(QString network, QString sender, QString file)
 * @param network Network name.
 * @param sender  Sender's name.
 * @param file    Filename.
 */

/**
 * Signal is emitted when line completed command has been received.
 *
 * @fn void Session::lineCompleted(QString network, QString chat, QString line)
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Completed line.
 */

/**
 * Signal is emitted when alert command has been received.
 *
 * @fn void Session::showAlert(QString network, QString sender, QString type)
 * @param network Network name.
 * @param sender  Triggerer of alert.
 * @param type    Alert type.
 */

/**
 * Signal is emitted when close alert command has been received.
 *
 * @fn void Session::closeAlert(QString network, QString sender, QString type)
 * @param network Network name.
 * @param sender  Triggerer of alert.
 * @param type    Alert type.
 */
