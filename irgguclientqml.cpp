/****************************************************************************
**  irgguclientqml.cpp
**
**  Copyright information
**
**      Copyright (C) 2013-2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "dbus/dbushandler.h"
#include "other/systray.h"
#include "other/notify.h"
#include "irgguclientqml.h"
#include "misc.h"
#include "settings.h"
#include "session.h"
#include <QApplication>
#include <QListIterator>
#include <QClipboard>
#include <QDebug>

/**
 * Constructor.
 *
 * @param *parent Pointer to parent.
 */
IrGGuClientQml::IrGGuClientQml(QObject *parent) : IrGGuClient(parent)
{
    engine    = new QQmlEngine();
    component = new QQmlComponent(engine);

    Misc     *misc     = new Misc(this);
    Settings *settings = new Settings(this);
    Session  *session  = new Session(this);
    Systray  *systray  = new Systray(this);
    Notify   *notify   = new Notify(this);

    engine->rootContext()->setContextProperty("client", this);
    engine->rootContext()->setContextProperty("settings", settings);
    engine->rootContext()->setContextProperty("session", session);
    engine->rootContext()->setContextProperty("systray", systray);
    engine->rootContext()->setContextProperty("notify", notify);
    component->loadUrl(QUrl("qrc:/qml/MainWindow.qml"));

    window = qobject_cast<QQuickWindow*>(component->create());

    qApp->setQuitOnLastWindowClosed(true);

    //qDebug() << component->errors() << endl;

    connect(engine, SIGNAL(quit()), qApp, SLOT(quit()));
    connect(session, SIGNAL(startLogin(QString,QString,QString,bool,bool,int,int)), this,
            SLOT(startLogin(QString,QString,QString,bool,bool,int,int)));
    connect(session, SIGNAL(startLogout()), this, SLOT(startLogout()));
    connect(session, SIGNAL(startSession(bool)), this, SLOT(startSession(bool)));
    connect(session, SIGNAL(reconnect()), this, SLOT(reconnect()));
    connect(session, SIGNAL(writeLine(QString,QString,QString)), this,
            SLOT(writeLine(QString,QString,QString)));
    connect(session, SIGNAL(completeLine(QString,QString,QString,int)), this,
            SLOT(completeLine(QString,QString,QString,int)));
    connect(session, SIGNAL(writeSetting(QString,QString,QString)), this,
            SLOT(writeSetting(QString,QString,QString)));
    connect(session, SIGNAL(writeDccFileReply(QString,QString,QString,bool)), this,
            SLOT(writeDccFileReply(QString,QString,QString,bool)));
    connect(session, SIGNAL(setIdle(bool,bool,int)), this, SLOT(setIdle(bool,bool,int)));
    connect(session, SIGNAL(quitNetwork(QString)), this, SLOT(quitNetwork(QString)));
    connect(session, SIGNAL(closeChat(QString,QString)), this, SLOT(closeChat(QString,QString)));
    connect(session, SIGNAL(query(QString,QString)), this, SLOT(query(QString,QString)));
    connect(this, SIGNAL(connected()), session, SLOT(handleConnected()));
    connect(this, SIGNAL(disconnected()), session, SLOT(handleDisconnected()));
    connect(this, SIGNAL(unableToConnect()), session, SLOT(handleUnableToConnect()));
    connect(this, SIGNAL(loginResult(int,int,int)), session, SLOT(handleLoginResult(int,int,int)));
    connect(this, SIGNAL(insertNetwork(QString)), session, SLOT(handleInsertNetwork(QString)));
    connect(this, SIGNAL(insertChat(QString,QString)), session,
            SLOT(handleInsertChat(QString,QString)));
    connect(this, SIGNAL(insertNick(QString,QString,QString)), session,
            SLOT(handleInsertNick(QString,QString,QString)));
    connect(this, SIGNAL(insertNicks(QString,QString,QStringList)), session,
            SLOT(handleInsertNicks(QString,QString,QStringList)));
    connect(this, SIGNAL(removeNetwork(QString)), session, SLOT(handleRemoveNetwork(QString)));
    connect(this, SIGNAL(removeChat(QString,QString)), session,
            SLOT(handleRemoveChat(QString,QString)));
    connect(this, SIGNAL(removeNick(QString,QString,QString)), session,
            SLOT(handleRemoveNick(QString,QString,QString)));
    connect(this, SIGNAL(changeNick(QString,QString,QString,QString)), session,
            SLOT(handleChangeNick(QString,QString,QString,QString)));
    connect(this, SIGNAL(changeNickMode(QString,QString,QString,QString)), session,
            SLOT(handleChangeNickMode(QString,QString,QString,QString)));
    connect(this, SIGNAL(renameChat(QString,QString,QString)), session,
            SLOT(handleRenameChat(QString,QString,QString)));
    connect(this, SIGNAL(newMsg(QString,QString,Line)), session,
            SLOT(handleNewMsg(QString,QString,Line)));
    connect(this, SIGNAL(newHighlightMsg(QString,QString,Line)), session,
            SLOT(handleNewHighlightMsg(QString,QString,Line)));
    connect(this, SIGNAL(newOwnMsg(QString,QString,Line)), session,
            SLOT(handleNewOwnMsg(QString,QString,Line)));
    connect(this, SIGNAL(appendMsg(QString,QString,Line)), session,
            SLOT(handleAppendMsg(QString,QString,Line)));
    connect(this, SIGNAL(newDCCFile(QString,QString,QString)), session,
            SLOT(handleNewDCCFile(QString,QString,QString)));
    connect(this, SIGNAL(closeDCCFile(QString,QString,QString)), session,
            SLOT(handleCloseDCCFile(QString,QString,QString)));
    connect(this, SIGNAL(lineCompleted(QString,QString,QString)), session,
            SLOT(handleLineCompleted(QString,QString,QString)));
    connect(this, SIGNAL(showAlert(QString,QString,int)), session,
            SLOT(handleShowAlert(QString,QString,int)));
    connect(this, SIGNAL(closeAlert(QString,QString,int)), session,
            SLOT(handleCloseAlert(QString,QString,int)));

    connectScreensaverSignal();

    session->setGUI(window);
}

/**
 * Copies text to clipboard.
 *
 * @param text Text that needs to be copied.
 */
void IrGGuClientQml::copyToClipboard(QString text)
{
    qApp->clipboard()->setText(text);
}

/**
 * Connects screensaverStateChanged signal to D-Bus ScreenSaver ActiveChanged signal.
 */
void IrGGuClientQml::connectScreensaverSignal ()
{
    #ifdef Q_OS_UNIX
    QString bus       = "session";
    QString service   = "org.freedesktop.ScreenSaver";
    QString path      = "/ScreenSaver";
    QString interface = "org.freedesktop.ScreenSaver";
    QString signal    = "ActiveChanged";

    DBusHandler<> dbusHandler;

    dbusHandler.connectDBusSignal(bus, service, path, interface, signal, this,
                                  SIGNAL(screensaverStateChanged(bool)));
    #endif
}

/**
 * This signal is emitted when active state of the screensaver has changed.
 *
 * @fn IrGGuClientQml::screensaverStateChanged(bool active)
 * @param active Active state of the screensaver.
 */
