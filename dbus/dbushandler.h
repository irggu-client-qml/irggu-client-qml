/****************************************************************************
**  dbushandler.h
**
**  Copyright information
**
**      Copyright (C) 2011,2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef DBUSHANDLER_H
#define DBUSHANDLER_H

#include <QtDBus>

/**
 *  This class is for using DBus.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2014-06-18
 */
template<typename TP1 = void, typename TP2 = void, typename TP3 = void, typename TP4 = void,
typename TP5 = void, typename TP6 = void, typename TP7 = void, typename TP8 = void>
class DBusHandler : public QObject
{
public:

    explicit DBusHandler(QObject *parent = 0);

    #ifdef Q_OS_UNIX


    bool dbusCall (QString bus, QString service, QString path, QString interface, QString method,
                   QList<QVariant> arguments = QList<QVariant>());
    QList<QVariant> dbusReplyCall (QString bus, QString service, QString path, QString interface,
                                   QString method, QList<QVariant> arguments = QList<QVariant>());
    bool connectDBusSignal (QString bus, QString service, QString path, QString interface,
                            QString signal, QObject *object, const char *slot);
    QDBusConnection getConnection (QString bus);

    #endif
};

#include "dbushandler.tpp"

#endif // DBUSHANDLER_H
