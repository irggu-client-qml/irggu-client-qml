/****************************************************************************
**  dbushandler.tpp
**
**  Copyright information
**
**      Copyright (C) 2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef DBUSHANDLER_TPP
#define DBUSHANDLER_TPP

#include <QString>
#include <QVariant>
#include <QDBusMessage>
#include <QDBusConnection>

/**
 * Constructor.
 *
 * @param *parent Pointer to parent.
 */
template<typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8>
DBusHandler<TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8>::DBusHandler (QObject *parent) : QObject(parent)
{

}

#ifdef Q_OS_UNIX

/**
 * Call D-Bus method.
 *
 * @param bus       D-Bus bus type.
 * @param service   D-Bus service.
 * @param path      D-Bus path.
 * @param interface D-Bus interface.
 * @param method    D-Bus method.
 * @param arguments D-Bus method arguments.
 * @return Was message queued successfully.
 */
template<typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8>
bool DBusHandler<TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8>::dbusCall (QString bus, QString service, QString path, QString interface, QString method, QList<QVariant> arguments)
{
    bool ok          = false;
    QDBusMessage msg = QDBusMessage::createMethodCall(service, path, interface, method);

    if ( !arguments.isEmpty() )
    {
        msg.setArguments(arguments);
    }

    ok = getConnection(bus).send(msg);

    return ok;
}

/**
 * Call D-Bus method with waiting for reply.
 *
 * @param bus       D-Bus bus type.
 * @param service   D-Bus service.
 * @param path      D-Bus path.
 * @param interface D-Bus interface.
 * @param method    D-Bus method.
 * @param arguments D-Bus method arguments.
 * @return Reply.
 */
template<typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8>
QList<QVariant> DBusHandler<TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8>::dbusReplyCall (QString bus, QString service, QString path, QString interface, QString method, QList<QVariant> arguments)
{
    QList<QVariant> replyList;
    QDBusMessage msg = QDBusMessage::createMethodCall(service, path, interface, method);
    QDBusPendingReply<TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8> reply;

    if ( !arguments.isEmpty() )
    {
        msg.setArguments(arguments);
    }

    reply = getConnection(bus).call(msg);

    if ( reply.isValid() )
    {
        int x = reply.count();

        for ( int i = 0; i < x; i++ )
        {
            replyList.append(reply.argumentAt(i));
        }
    }

    return replyList;
}

/**
 * Connect slot to D-Bus signal.
 *
 * @param bus       D-Bus bus type.
 * @param service   D-Bus service.
 * @param path      D-Bus path.
 * @param interface D-Bus interface.
 * @param signal    D-Bus signal.
 * @param *object   Pointer to object.
 * @param *slot     Pointer to slot.
 * @return Was connection successful.
 */
template<typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8>
bool DBusHandler<TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8>::connectDBusSignal (QString bus, QString service, QString path, QString interface, QString signal, QObject *object, const char *slot)
{
    bool ok = getConnection(bus).connect(service, path, interface, signal, object,
                                                          slot);
    return ok;
}

template<typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8>
/**
 * Gets D-Bus connection.
 *
 * @param bus Specifies the bus type.
 * @return D-Bus connection.
 */
QDBusConnection DBusHandler<TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8>::getConnection (QString bus)
{
    if ( bus == "system" )
    {
        return QDBusConnection::systemBus();
    }
    else
    {
        return QDBusConnection::sessionBus();
    }
}

#endif

#endif // DBUSHANDLER_TPP
