/****************************************************************************
**  misc.h
**
**  Copyright information
**
**      Copyright (C) 2013-2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef MISC_H
#define MISC_H

#include <QObject>

/**
 *  This class is for miscellaneous.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2014-11-14
 */
class Misc : public QObject
{
    Q_OBJECT
public:
    explicit Misc(QObject *parent = 0);

    bool hasNotifyService();

    static Misc *getInstance();

private:
    bool notifyService;

    static Misc *instance;
};

#endif // MISC_H
