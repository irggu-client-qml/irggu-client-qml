/****************************************************************************
**  irgguclientqml.h
**
**  Copyright information
**
**      Copyright (C) 2013-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef IRGGUCLIENTQML_H
#define IRGGUCLIENTQML_H

#include <irggu-client/irgguclient.h>
#include <QObject>
#include <QtQml>
#include <QtQuick/QQuickView>

/**
 *  This class is the main class.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2014-10-20
 */
class IrGGuClientQml : public IrGGuClient
{
    Q_OBJECT
public:
    explicit IrGGuClientQml(QObject *parent = 0);

    Q_INVOKABLE void copyToClipboard(QString text);

private:
    QQmlEngine    *engine;
    QQmlComponent *component;
    QQuickWindow  *window;

    void connectScreensaverSignal();

Q_SIGNALS:
    void screensaverStateChanged(bool active);

};

#endif // IRGGUCLIENTQML_H
