/****************************************************************************
**  settings.h
**
**  Copyright information
**
**      Copyright (C) 2013-2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef SETTINGS_H
#define SETTINGS_H

#include "misc.h"
#include <QObject>
#include <QSettings>
#include <QVariantMap>
#include <QFont>

typedef QMap<QString, QVariantMap> SettingsList;

/**
 *  This class is for handling settings.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2014-11-16
 */
class Settings : public QObject
{
    Q_OBJECT
public:
    explicit Settings(QObject *parent = 0);

    Q_INVOKABLE int     getInt(QString type, QString setting);
    Q_INVOKABLE QString getString(QString type, QString setting);
    Q_INVOKABLE QFont   getFont(QString type, QString setting);
    Q_INVOKABLE bool    getBool(QString type, QString setting);
    Q_INVOKABLE void    setInt(QString type, QString setting, int value);
    Q_INVOKABLE void    setString(QString type, QString setting, QString value);
    Q_INVOKABLE void    setFont(QString type, QString setting, QFont value);
    Q_INVOKABLE void    setBool(QString type, QString setting, bool value);
    Q_INVOKABLE void    writeLoginSettings();
    Q_INVOKABLE void    writeUserSettings();
    Q_INVOKABLE void    readUserSettings(QString server, QString username, bool temporary);

    static Settings *getInstance();

private:
    Misc         *misc;
    SettingsList settings;
    QSettings    *userSettingsFile;
    QSettings    *loginSettingsFile;

    static QString  filename;
    static Settings *instance;

    void readLoginSettings();

Q_SIGNALS:
    void intSettingChanged(QString type, QString setting, int value);
    void stringSettingChanged(QString type, QString setting, QString value);
    void fontSettingChanged(QString type, QString setting, QFont value);
    void boolSettingChanged(QString type, QString setting, bool value);

};

#endif // SETTINGS_H
