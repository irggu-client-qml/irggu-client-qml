/****************************************************************************
**  systray.h
**
**  Copyright information
**
**      Copyright (C) 2013-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef SYSTRAY_H
#define SYSTRAY_H

#include "../settings.h"
#include <QSystemTrayIcon>
#include <QTimer>
#include <QMenu>
#include <QAction>

enum class IconType
{
    Normal,
    Alert
};

typedef QMap<QString, QAction*> actionList;

/**
 *  This class is for showing systray icon.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2015-02-10
 */
class Systray : public QSystemTrayIcon
{
    Q_OBJECT
public:
    explicit Systray(QObject *parent = 0);
    
    void startBlink();

    static Systray *getInstance();

private:
    Settings   *settings;
    QString    sType;
    QString    sName;
    QMenu      *systrayMenu;
    actionList actions;
    QIcon      normalIcon;
    QIcon      alertIcon;
    QTimer     blinkTimer;
    IconType   iconType;

    static Systray *instance;

Q_SIGNALS:
    void clicked();
    void menuClicked(QString action);

public Q_SLOTS:
    void stopBlink();

private Q_SLOTS:
    void changeIcon();
    void handleActivated(QSystemTrayIcon::ActivationReason reason);
    void handleMenuActived(QAction *action);
    void boolSettingChanged(QString type, QString setting, bool value);
    
};

#endif // SYSTRAY_H
