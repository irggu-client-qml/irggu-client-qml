/****************************************************************************
**  notify.cpp
**
**  Copyright information
**
**      Copyright (C) 2013-2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "notify.h"
#include <dbus/dbushandler.h>
#include <QVariant>

Notify  *Notify::instance                 = NULL;
Systray *Notify::systray                  = NULL;
QString Notify::service                   = "org.freedesktop.Notifications";
QString Notify::path                      = "/org/freedesktop/Notifications";
QString Notify::interface                 = "org.freedesktop.Notifications";
QString Notify::notifyMethod              = "Notify";
QString Notify::closeMethod               = "CloseNotification";
QString Notify::msg                       = "New private message from <network> <sender>";
QString Notify::highlight                 = "New highlight message from <network> <sender>";
QString Notify::dcc                       = "Incomming file from <network> <sender>";
NetworkSenderMap Notify::networkSenderMap = NetworkSenderMap();

void (*Notify::notifyType)(QString, QString, QString, QString) = NULL;
void (*Notify::closeType)(QString, QString, QString)           = NULL;

/**
 * Constructor.
 *
 * @param *parent Pointer to parent.
 */
Notify::Notify (QObject *parent) : QObject(parent)
{
    instance = this;
    misc     = Misc::getInstance();
    settings = Settings::getInstance();
    systray  = Systray::getInstance();

    alertTypes.insert("Highlight", AlertType::Highlight);
    alertTypes.insert("Private", AlertType::Priv);
    alertTypes.insert("DCC", AlertType::DCC);

    aType      = "Alert";
    aWASetting = "WindowActive";
    aWDSetting = "WindowDeactive";
    aWCSetting = "WindowClosed";
    aSNSetting = "ShowNotification";
    aBSSetting = "BlinkSystray";

    alertSettings.insert(aWASetting, AlertSetting::aWASetting);
    alertSettings.insert(aWDSetting, AlertSetting::aWDSetting);
    alertSettings.insert(aWCSetting, AlertSetting::aWCSetting);
    alertSettings.insert(aSNSetting, AlertSetting::aSNSetting);
    alertSettings.insert(aBSSetting, AlertSetting::aBSSetting);

    alertWindowActive   = false;
    alertWindowDeactive = false;
    alertWindowClosed   = false;
    showNotification    = false;
    blinkSystray        = false;

    if ( misc->hasNotifyService() )
    {
        notifyType = &notifyService;
        closeType  = &closeService;
    }
    else if ( systray->supportsMessages() )
    {
        notifyType = &notifySystray;
    }

    connect(settings, SIGNAL(boolSettingChanged(QString,QString,bool)), this,
            SLOT(boolSettingChanged(QString,QString,bool)));
}

/**
 * Notifies user.
 *
 * @param network Network name.
 * @param sender  Alerts sender.
 * @param type    Alert type.
 * @param current Specifies if alert is from current chat.
 */
void Notify::notify(QString network, QString sender, QString type, bool current)
{
    QString message;
    bool notification = false;
    bool blink        = false;

    if ( ( windowActive && alertWindowActive && !current )
         || ( !windowActive && alertWindowDeactive ) || ( windowClosed && alertWindowClosed ) )
    {
        notification = showNotification;
        blink        = blinkSystray;
    }

    if ( notification && !screensaverActive )
    {
        switch ( alertTypes.value(type) )
        {
            case AlertType::Priv:
            {
                message = msg.replace("<network>", network).replace("<sender>", sender);

                break;
            }

            case AlertType::Highlight:
            {
                message = highlight.replace("<network>", network).replace("<sender>", sender);

                break;
            }

            case AlertType::DCC:
            {
                message = dcc.replace("<network>", network).replace("<sender>", sender);
            }
        }

        if ( notifyType )
        {
            notifyType(message, network, sender, type);
        }

        lastAlert.network = network;
        lastAlert.sender  = sender;
        lastAlert.type    = type;
    }

    if ( blink )
    {
        systray->startBlink();
    }
}

/**
 * Closes alert.
 *
 * @param network Network name.
 * @param sender  Alerts sender.
 * @param type    Alert type.
 */
void Notify::close(QString network, QString sender, QString type)
{
    if ( lastAlert.network == network && lastAlert.sender == sender && lastAlert.type == type )
    {
        systray->stopBlink();

        lastAlert.network = "";
        lastAlert.sender  = "";
        lastAlert.type    = "";
    }

    if ( closeType )
    {
        closeType(network, sender, type);
    }
}

/**
 * Called when window active status changes.
 *
 * @param windowActive Specifies if window is active.
 */
void Notify::setWindowActive (bool windowActive)
{
    this->windowActive = windowActive;

    if ( windowActive )
    {
        systray->stopBlink();
    }
}

/**
 * Called when window visibility changes.
 *
 * @param windowClosed Specifies window's is visibilty state.
 */
void Notify::setWindowClosed (bool windowClosed)
{
    this->windowClosed = windowClosed;
}

/**
 * Called when screensaver active state changes.
 *
 * @param screensaverActive Specifies screensaver's is active state.
 */
void Notify::setScreensaverActive (bool screensaverActive)
{
    this->screensaverActive = screensaverActive;
}

/**
 * Gets the instance.
 *
 * @return Instance.
 */
Notify *Notify::getInstance()
{
    return instance;
}

/**
 * Shows notification via D-Bus service.
 *
 * @param message Notification message.
 * @param network Network name.
 * @param sender  Alerts sender.
 * @param type    Alert type.
 */
void Notify::notifyService (QString message, QString network, QString sender, QString type)
{
    quint32 id           = 0;
    bool    networkFound = false;
    bool    senderFound  = false;

    if ( networkSenderMap.find(network) != networkSenderMap.end() )
    {
        networkFound = true;

        SenderTypeMap st = networkSenderMap.value(network);

        if ( st.find(sender) != st.end() )
        {
            senderFound = true;

            TypeIDMap ti = st.value(sender);

            id = ti.value(type, 0);
        }
    }

    QList<QVariant> arguments;
    arguments.append(QVariant(QString("IrGGu")));
    arguments.append(QVariant(id));
    arguments.append(QVariant(QString("irggu")));
    arguments.append(QVariant("IrGGu"));
    arguments.append(QVariant(message));
    arguments.append(QVariant(QStringList()));
    arguments.append(QVariant(QVariantMap()));
    arguments.append(QVariant((int)0));

    DBusHandler<quint32> dbusHandler(instance);

    QList<QVariant> reply = dbusHandler.dbusReplyCall("session", service, path, interface,
                                                      notifyMethod, arguments);

    if ( !reply.isEmpty() )
    {
        id = reply.at(0).toUInt();

        if ( networkFound )
        {
            SenderTypeMap st = networkSenderMap.value(network);

            if ( senderFound )
            {
                TypeIDMap ti = st.value(sender);

                ti.insert(type, id);
            }
            else
            {
                TypeIDMap ti;

                ti.insert(type, id);
                st.insert(sender, ti);
            }
        }
        else
        {
            TypeIDMap ti;
            SenderTypeMap st;

            ti.insert(type, id);
            st.insert(sender, ti);

            networkSenderMap.insert(network, st);
        }
    }
}

/**
 * Shows notification via systray icon.
 *
 * @param message Notification message.
 * @param network Network name.
 * @param sender  Alerts sender.
 * @param type    Alert type.
 */
void Notify::notifySystray (QString message, QString network, QString sender, QString type)
{
    systray->showMessage("IrGGu", message);
}

/**
 * Closes notification via D-Bus service.
 *
 * @param network Network name.
 * @param sender  Alerts sender.
 * @param type    Alert type.
 */
void Notify::closeService (QString network, QString sender, QString type)
{
    if ( networkSenderMap.find(network) != networkSenderMap.end() )
    {
        SenderTypeMap st = networkSenderMap.value(network);

        if ( st.find(sender) != st.end() )
        {
            TypeIDMap ti = st.value(sender);

            if ( ti.find(type) != ti.end() )
            {
                quint32 id = ti.take(type);

                QList<QVariant> arguments;
                arguments.append(QVariant(id));

                DBusHandler<> dbusHandler(instance);

                dbusHandler.dbusCall("session", service, path, interface, closeMethod, arguments);
            }
        }
    }
}

/**
 * Called when boolean setting has changed.
 *
 * @param type    Setting type.
 * @param setting Setting name.
 * @param value   Setting value.
 */
void Notify::boolSettingChanged(QString type, QString setting, bool value)
{
    if ( type == aType )
    {
        switch ( alertSettings.value(setting) )
        {
            case AlertSetting::aWASetting:
            {
                alertWindowActive = value;

                break;
            }

            case AlertSetting::aWDSetting:
            {
                alertWindowDeactive = value;

                break;
            }

            case AlertSetting::aWCSetting:
            {
                alertWindowClosed = value;

                break;
            }

            case AlertSetting::aSNSetting:
            {
                showNotification = value;

                break;
            }

            case AlertSetting::aBSSetting:
            {
                blinkSystray = value;

                break;
            }
        }
    }
}
