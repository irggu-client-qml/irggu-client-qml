/****************************************************************************
**  notify.h
**
**  Copyright information
**
**      Copyright (C) 2013-2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef NOTIFY_H
#define NOTIFY_H

#include "../settings.h"
#include "../misc.h"
#include "systray.h"
#include <QObject>

enum class AlertType
{
    Highlight = 1,
    Priv,
    DCC
};

enum class AlertSetting
{
    aWASetting = 1,
    aWDSetting,
    aWCSetting,
    aSNSetting,
    aBSSetting
};

struct Alert
{
    QString network;
    QString sender;
    QString type;
};

typedef QMap<QString, AlertType> AlertTypes;
typedef QMap<QString, AlertSetting> AlertSettings;
typedef QMap<QString, quint32> TypeIDMap;
typedef QMap<QString, TypeIDMap> SenderTypeMap;
typedef QMap<QString, SenderTypeMap> NetworkSenderMap;

/**
 *  This class is for notifying user.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2014-06-18
 */
class Notify : public QObject
{
    Q_OBJECT
public:
    explicit Notify(QObject *parent = 0);

    Q_INVOKABLE void notify(QString network, QString sender, QString type, bool current);
    Q_INVOKABLE void close(QString network, QString sender, QString type);
    Q_INVOKABLE void setWindowActive(bool windowActive);
    Q_INVOKABLE void setWindowClosed(bool windowClosed);
    Q_INVOKABLE void setScreensaverActive(bool screensaverActive);

    static Notify *getInstance();

private:
    Misc          *misc;
    Settings      *settings;
    AlertTypes    alertTypes;
    QString       aType;
    QString       aWASetting;
    QString       aWDSetting;
    QString       aWCSetting;
    QString       aSNSetting;
    QString       aBSSetting;
    AlertSettings alertSettings;
    Alert         lastAlert;
    bool          windowActive;
    bool          windowClosed;
    bool          screensaverActive;
    bool          alertWindowActive;
    bool          alertWindowDeactive;
    bool          alertWindowClosed;
    bool          showNotification;
    bool          blinkSystray;


    static Notify           *instance;
    static Systray          *systray;
    static QString          service;
    static QString          path;
    static QString          interface;
    static QString          notifyMethod;
    static QString          closeMethod;
    static QString          msg;
    static QString          highlight;
    static QString          dcc;
    static NetworkSenderMap networkSenderMap;

    static void (*notifyType)(QString, QString, QString, QString);
    static void (*closeType)(QString, QString, QString);
    
    static void notifyService(QString message, QString network, QString sender, QString type);
    static void notifySystray(QString message, QString network, QString sender, QString type);
    static void closeService(QString network, QString sender, QString type);

private Q_SLOTS:
    void boolSettingChanged(QString type, QString setting, bool value);
};

#endif // NOTIFY_H
