/****************************************************************************
**  systray.cpp
**
**  Copyright information
**
**      Copyright (C) 2013-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "systray.h"

Systray *Systray::instance = NULL;

/**
 * Constructor.
 *
 * @param *parent Pointer to parent.
 */
Systray::Systray (QObject *parent) : QSystemTrayIcon(parent)
{
    instance    = this;
    settings    = Settings::getInstance();
    sType       = "Systray";
    sName       = "ShowSystrayIcon";
    systrayMenu = new QMenu();

    actions.insert("Show/Hide", new QAction("Show/Hide", systrayMenu));
    actions.insert("Logout", new QAction("Logout", systrayMenu));
    actions.insert("Quit", new QAction("Quit", systrayMenu));

    systrayMenu->addAction(actions.value("Show/Hide"));
    systrayMenu->addAction(actions.value("Logout"));
    systrayMenu->addAction(actions.value("Quit"));
    setContextMenu(systrayMenu);

    normalIcon = QIcon::fromTheme("irggu", QIcon(":/icons/irggu.svg"));
    alertIcon  = QIcon::fromTheme("irggu-systray-blink", QIcon(":/icons/systray-blink.svg"));
    iconType   = IconType::Normal;

    blinkTimer.setInterval(500);

    setIcon(normalIcon);

    connect(settings, SIGNAL(boolSettingChanged(QString,QString,bool)), this,
            SLOT(boolSettingChanged(QString,QString,bool)));
    connect(&blinkTimer, SIGNAL(timeout()), this, SLOT(changeIcon()));
    connect(this, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this,
            SLOT(handleActivated(QSystemTrayIcon::ActivationReason)));
    connect(systrayMenu, SIGNAL(triggered(QAction*)), this, SLOT(handleMenuActived(QAction*)));
}

/**
 * Starts blinking.
 */
void Systray::startBlink ()
{
    blinkTimer.start();
}

/**
 * Gets instance.
 *
 * @return Instance.
 */
Systray *Systray::getInstance()
{
    return instance;
}

/**
 * Stops blinking.
 */
void Systray::stopBlink ()
{
    blinkTimer.stop();

    if ( iconType == IconType::Alert )
    {
        iconType = IconType::Normal;

        setIcon(normalIcon);
    }
}

/**
 * Changes systray icon.
 */
void Systray::changeIcon ()
{
    if ( iconType == IconType::Normal )
    {
        iconType = IconType::Alert;

        setIcon(alertIcon);
    }
    else
    {
        iconType = IconType::Normal;

        setIcon(normalIcon);
    }
}

/**
 * Called when systray icon has been clicked.
 *
 * @param reason How systray was activated.
 */
void Systray::handleActivated(QSystemTrayIcon::ActivationReason reason)
{
    stopBlink();

    if ( reason == QSystemTrayIcon::Trigger )
    {
        Q_EMIT clicked();
    }
}

/**
 * Called when entry in systray menu has been clicked.
 *
 * @param action Action thas was clicked.
 */
void Systray::handleMenuActived(QAction *action)
{
    Q_EMIT menuClicked(actions.key(action));
}

/**
 * Called when boolean setting has changed.
 *
 * @param type    Setting type.
 * @param setting Setting name.
 * @param value   Setting value.
 */
void Systray::boolSettingChanged(QString type, QString setting, bool value)
{
    if ( type == sType && setting == sName )
    {
        setVisible(value);
    }
}

/**
 * This signal is emitted when systray icon has been clicked.
 *
 * @fn Systray::clicked()
 */

/**
 * This signal is emitted when entry in systray menu has been clicked.
 *
 * @fn Systray::menuClicked(QString action)
 * @param action Action thas was clicked.
 */
