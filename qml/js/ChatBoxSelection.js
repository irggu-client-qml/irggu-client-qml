/****************************************************************************
**  ChatBoxSelection.js
**
**  Copyright information
**
**      Copyright (C) 2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

/**
 * @class   ChatBoxSelection
 * @author  Arttu Liimola
 * @version 2014-11-14
 *
 * Javascript to handle text selection.
 */

var selectionStarted = false;
var startIndex       = 0;
var startX           = 0;
var startY           = 0;
var selected         = [];
var skipLink         = false;
var chatBoxListView  = null;
var selectTimestamp  = false;
var sType            = "Selecting";
var sSetting         = "SelectTimestamp";

/**
 * Sets the view where text is selected.
 *
 * @memberof ChatBoxSelection
 * @param ChatBoxListView view View.
 */
function setView (view)
{
    chatBoxListView = view;
}

/**
 * Opens link if user clicked it.
 *
 * @memberof ChatBoxSelection
 * @param MouseEvent mouse Mouse event.
 */
function mouseClicked (mouse)
{
    if ( chatBoxListView && !skipLink )
    {
        var x    = mouse.x;
        var y    = mouse.y + chatBoxListView.contentY;
        var item = chatBoxListView.itemAt(x, y);

        if ( item )
        {
            var textEdit = item.childAt(x, 0);

            if ( textEdit.objectName === "msg" )
            {
                if ( textEdit.hoveredLink )
                {
                    Qt.openUrlExternally(textEdit.hoveredLink);
                }
            }
        }
    }
    else
    {
        skipLink = false;
    }
}

/**
 * Starts text selection.
 *
 * @memberof ChatBoxSelection
 * @param MouseEvent mouse Mouse event.
 */
function mousePressed (mouse)
{
    if ( chatBoxListView )
    {
        var x    = mouse.x;
        var y    = mouse.y + chatBoxListView.contentY;
        var item = chatBoxListView.itemAt(x, y);

        startIndex = item ? chatBoxListView.indexAt(x, y) : ( chatBoxListView.model.count - 1 );
        startX     = x;
        startY     = y;

        selectionStarted = true;
    }
}

/**
 * Updates text selection.
 *
 * @memberof ChatBoxSelection
 * @param MouseEvent mouse Mouse event.
 */
function mouseMove (mouse)
{
    if ( chatBoxListView )
    {
        var x = mouse.x;
        var y = mouse.y + chatBoxListView.contentY;

        if ( y < 0 )
        {
            x = 0;
            y = 0;
        }

        if ( x < 0 )
        {
            x = 0;
        }
        else if ( x >= chatBoxListView.width )
        {
            x = chatBoxListView.width - 1;
        }

        var item = chatBoxListView.itemAt(x, y);

        if ( item )
        {
            if ( selectionStarted )
            {
                select(chatBoxListView.indexAt(x, y), x, y);
            }
            else
            {
                var textEdit = item.childAt(x, 0);

                if ( textEdit )
                {
                    var cursor = textEdit.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor;

                    chatBoxMouseArea.cursorShape = cursor;
                }
            }
        }
        else if ( selectionStarted )
        {
            x = chatBoxListView.width;

            select(( chatBoxListView.model.count - 1 ), x, y);
        }
    }
}

/**
 * Ends text selection.
 *
 * @memberof ChatBoxSelection
 * @param MouseEvent mouse Mouse event.
 */
function mouseReleased (mouse)
{
    if ( chatBoxListView && selectionStarted )
    {
        selectionStarted = false;

        var line        = "";
        var text        = "";
        var textNewLine = "";

        for ( var i in selected )
        {
            line         = getSelectedText(selected[i]).replace(/\s/g,' ');
            text        += line;
            textNewLine += line + "\n";

        }

        if ( text.trim().length > 0 )
        {
            skipLink = true;

            client.copyToClipboard(textNewLine);
        }

        deselect();
    }
}

/**
 * Selects text from start position to end.
 *
 * @memberof ChatBoxSelection
 * @param integer endIndex End index.
 * @param integer endX     End x pos.
 * @param integer endY     End y pos.
 */
function select (endIndex, endX, endY)
{
    if ( chatBoxListView )
    {
        deselect();

        var index = startIndex;
        var sI    = false;
        var eI    = false;
        var sX    = 0;
        var eX    = 0;
        var sY    = 0;
        var eY    = 0;
        var item  = null;

        if ( index < endIndex || ( index === endIndex && startX < endX ) )
        {
            for (; index <= endIndex; index++ )
            {
                chatBoxListView.currentIndex = index;

                item = chatBoxListView.currentItem;

                sI = index === startIndex;
                eI = index === endIndex;
                sX = sI ? startX : 0;
                eX = eI ? endX : chatBoxListView.width;
                sY = sI ? ( startY - item.y ) : 0;
                eY = eI ? ( endY - item.y ) : item.height;

                selectIndex(index, sX, eX, sY, eY);

                selected.push(index);
            }
        }
        else if ( index >= endIndex)
        {
            for (; index >= endIndex; index-- )
            {
                chatBoxListView.currentIndex = index;

                item = chatBoxListView.currentItem;

                sI = index === startIndex;
                eI = index === endIndex;
                sX = eI ? endX : 0;
                eX = sI ? startX : chatBoxListView.width;
                sY = eI ?  ( endY - item.y ) : 0;
                eY = sI ? ( startY - item.y ) : item.height;

                selectIndex(index, sX, eX, sY, eY);

                selected.push(index);
            }

            selected.reverse();
        }
    }
}

/**
 * Deselects selected text.
 *
 * @memberof ChatBoxSelection
 */
function deselect ()
{
    for ( var i in selected )
    {
        deselectIndex(selected[i]);
    }

    selected = [];
}

/**
 * Selects text at specific index.
 *
 * @memberof ChatBoxSelection
 * @param integer index  Index of ChatBoxListView.
 * @param integer startX X pos where to start the selection.
 * @param integer endX   X pos where to end the selection.
 * @param integer startY Y pos where to start the selection.
 * @param integer endY   Y pos where to end the selection.
 */
function selectIndex (index, startX, endX, startY, endY)
{
    if ( chatBoxListView )
    {
        var item      = chatBoxListView.currentItem;
        var first     = item.childAt(startX, 0);
        var timestamp = item.children[0];
        var nick      = item.children[2];
        var msg       = item.children[4];

        if ( first )
        {
            switch ( first.objectName )
            {
                case "timestamp":
                {
                    if ( endX > timestamp.x && selectTimestamp )
                    {
                        var start = timestamp.positionAt(startX, 0);
                        var end   = timestamp.positionAt(endX, 0);

                        timestamp.select(start, end);
                    }
                }

                case "whitespace1":
                case "nick":
                {
                    if ( endX > nick.x )
                    {
                        var start = nick.positionAt(( startX - nick.x ), 0);
                        var end   = nick.positionAt(( endX - nick.x ), 0);

                        nick.select(start, end);
                    }
                }

                case "whitespace2":
                case "msg":
                {
                    if ( endX > msg.x )
                    {
                        var start = msg.positionAt(( startX - msg.x ), startY);
                        var end   = msg.positionAt(( endX - msg.x ), endY);

                        msg.select(start, end);
                    }
                }
            }
        }
    }
}

/**
 * Deselects text at specific index.
 *
 * @memberof ChatBoxSelection
 * @param integer index Index of ChatBoxListView.
 */
function deselectIndex (index)
{
    if ( chatBoxListView )
    {
        chatBoxListView.currentIndex = index;

        var item      = chatBoxListView.currentItem;
        var timestamp = item.children[0];
        var nick      = item.children[2];
        var msg       = item.children[4];

        timestamp.deselect();
        nick.deselect();
        msg.deselect();
    }
}

/**
 * Gets selected text at specific index.
 *
 * @memberof ChatBoxSelection
 * @param integer index Index of ChatBoxListView.
 * @return string Selected text.
 */
function getSelectedText (index)
{
    if ( chatBoxListView )
    {
        chatBoxListView.currentIndex = index;

        var item      = chatBoxListView.currentItem;
        var timestamp = item.children[0].selectedText;
        var nick      = item.children[2].selectedText;
        var msg       = item.children[4].selectedText;
        var text      = timestamp + ' ' + nick + ' ' + msg;

        return text;
    }
}

/**
 * Called when boolean setting has changed.
 *
 * @memberof ChatBoxSelection
 * @param string  type    Setting type.
 * @param string  setting Setting name.
 * @param boolean value   Setting value.
 */
function boolSettingChanged (type, setting, value)
{
    if ( type === sType && setting === sSetting )
    {
        selectTimestamp = value;
    }
}
