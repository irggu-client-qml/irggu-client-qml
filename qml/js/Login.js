/****************************************************************************
**  Login.js
**
**  Copyright information
**
**      Copyright (C) 2013-2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

/**
 * @class   Login
 * @author  Arttu Liimola
 * @version 2014-06-17
 *
 * Javascript to handle login.
 */

var dialogVisible;

/**
 * Initialize.
 *
 * @memberof Login
 */
function init ()
{
    dialogVisible = false;
}

/**
 * Gets values for login dialog from settings.
 *
 * @memberof Login
 */
function getSettings ()
{
    server.text              = settings.getString("Login", "Server");
    username.text            = settings.getString("Login", "Username");
    password.text            = settings.getString("Login", "Password");
    useSsl.checked           = settings.getBool("Login", "UseSsl");
    ignoreSslErrors.checked  = settings.getBool("Login", "IgnoreSslErrors");
    rememberMe.checked       = settings.getBool("Login", "RememberMe");
    rememberPassword.checked = settings.getBool("Login", "RememberPassword");
    loginOnStartup.checked   = settings.getBool("Login", "LoginOnStartup");
}

/**
 * Called when the visible state of the login dialog has changed.
 *
 * @memberof Login
 * @param boolean visible Visible state of the login dialog.
 */
function visibleChanged (visible)
{
    dialogVisible = visible;

    if ( dialogVisible )
    {
        loginDialog.title = qsTr("Login");

        getSettings();
    }
}

/**
 * Called when session is ready for login.
 *
 * @memberof Login
 */
function sessionReady ()
{
    if ( dialogVisible && loginOnStartup.checked )
    {
        startLogin();
    }
}

/**
 * Called when use SSL checkbox has been clicked.
 *
 * @memberof Login
 */
function useSslChanged ()
{
    if ( useSsl.checked )
    {
        ignoreSslErrors.enabled = true;
    }
    else
    {
        ignoreSslErrors.checked = false;
        ignoreSslErrors.enabled = false;
    }
}

/**
 * Called when remember me checkbox has been clicked.
 *
 * @memberof Login
 */
function rememberMeChanged ()
{
    if ( rememberMe.checked )
    {
        rememberPassword.enabled = true;
    }
    else
    {
        rememberPassword.checked = false;
        rememberPassword.enabled = false;
    }
}

/**
 * Called when remember password checkbox has been clicked.
 *
 * @memberof Login
 */
function rememberPasswordChanged ()
{
    if ( rememberPassword.checked )
    {
        loginOnStartup.enabled = true;
    }
    else
    {
        loginOnStartup.checked = false;
        loginOnStartup.enabled = false;
    }
}

/**
 * Called when login button has been clicked.
 *
 * @memberof Login
 */
function startLogin ()
{
    if ( string.isEmpty(server.text) )
    {
        loginDialog.title = qsTr("Server cannot be empty!");
    }
    else if ( !/^.+\/[1-9][0-9]*$/.test(server.text) )
    {
        loginDialog.title = qsTr("Write server in format host/port!");
    }
    else if ( string.isEmpty(username.text) )
    {
        loginDialog.title = qsTr("Username cannot be empty!");
    }
    else if ( string.isEmpty(password.text) )
    {
        loginDialog.title = qsTr("Password cannot be empty!");
    }
    else
    {
        loginDialog.title = qsTr("Connecting to server!");

        login(server.text, username.text, password.text, useSsl.checked, ignoreSslErrors.checked,
              !rememberMe.checked);
    }
}

/**
 * Starts logout.
 *
 * @memberof Login
 */
function startLogout ()
{
    logout();
}

/**
 * Called when client has connected to server.
 *
 * @memberof Login
 */
function connected ()
{
    if ( dialogVisible )
    {
        loginDialog.title = qsTr("Connected. Login on the server!");
    }
}

/**
 * Called when client was unable to connect to server.
 *
 * @memberof Login
 */
function unableToConnect ()
{
    if ( dialogVisible )
    {
        loginDialog.title = qsTr("Unable to connect to the server!");
    }
}

/**
 * Called when client receives login result.
 *
 * @memberof Login
 * @param string result Login result.
 */
function loginResult (result)
{
    if ( dialogVisible )
    {
        switch ( result )
        {
            case "noError":
            {
                if ( rememberMe.checked )
                {
                    settings.setString("Login", "Server", server.text);
                    settings.setString("Login", "Username", username.text);
                    settings.setString("Login", "Password", password.text);
                    settings.setBool("Login", "UseSsl", useSsl.checked);
                    settings.setBool("Login", "IgnoreSslErrors", ignoreSslErrors.checked);
                    settings.setBool("Login", "RememberMe", true);
                    settings.setBool("Login", "RememberPassword", rememberPassword.checked);
                    settings.setBool("Login", "LoginOnStartup", loginOnStartup.checked);

                    settings.writeLoginSettings();
                }

                logged();

                break;
            }

            case "wrongUserPass":
            {
                loginDialog.title = qsTr("Wrong username or password!");

                break;
            }

            case "maxClientLimit":
            {
                loginDialog.title = qsTr("Maximum client limit reached!");

                break;
            }
        }
    }
}
