/****************************************************************************
**  Dcc.js
**
**  Copyright information
**
**      Copyright (C) 2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

/**
 * @class   Dcc
 * @author  Arttu Liimola
 * @version 2014-07-05
 *
 * Javascript to display incomming DCC files and reply to accept or discard the file.
 */

/**
 * Clears DCC file list.
 *
 * @memberof Dcc
 */
function clear ()
{
    dccModel.clear();
}

/**
 * Insert new incomming DCC file to DCC dialog's table.
 *
 * @memberof Dcc
 * @param string network Network name.
 * @param string sender  Sender's name.
 * @param string file    Filename.
 */
function newDCCFile (network, sender, file)
{
    dccModel.append({"network": network, "sender": sender, "file": file});

    select(dccModel.count - 1);

    dccDialog.show();
}

/**
 * Remove incomming DCC file from the DCC dialog's table.
 *
 * @memberof Dcc
 * @param string network Network name.
 * @param string sender  Sender's name.
 * @param string file    Filename.
 */
function closeDCCFile (network, sender, file)
{
    var current = dccTableView.currentRow;
    var dcc;
    var index

    dccTableView.model = null;

    for ( var i = 0; i < dccModel.count; i++ )
    {
        dcc = dccModel.get(i);

        if ( dcc.network === network && dcc.sender === sender && dcc.file === file )
        {
            dccModel.remove(i);

            index = i;
        }
    }

    dccTableView.model = dccModel;

    if ( !dccModel.count )
    {
        acceptButton.enabled = false;
        discardButton.enabled = false;
    }
    else if ( index === current )
    {
        if ( index === dccModel.count )
        {
            index--;
        }

        select(index);
    }
    else
    {
        if ( current > index )
        {
            current--;
        }

        select(current);
    }
}

/**
 * Select incomming DCC file from the DCC dialog's table.
 *
 * @memberof Dcc
 * @param integer row Row on the DCC dialog's table.
 */
function select (row)
{
    dccTableView.currentRow = row;

    dccTableView.selection.clear();
    dccTableView.selection.select(row);

    clicked(row);
}

/**
 * Called when row has been clicked on the DCC dialog's table.
 *
 * @memberof Dcc
 * @param integer row Row on the DCC dialog's table.
 */
function clicked (row)
{
    acceptButton.enabled = true;
    discardButton.enabled = true;
}

/**
 * Called when accept button has been clicked on the DCC dialog.
 *
 * @memberof Dcc
 */
function accept ()
{
    var dcc = dccModel.get(dccTableView.currentRow);

    writeDccFileReply(dcc.network, dcc.sender, dcc.file, true);
}

/**
 * Called when discard button has been clicked on the DCC dialog.
 *
 * @memberof Dcc
 */
function discard ()
{
    var dcc = dccModel.get(dccTableView.currentRow);

    writeDccFileReply(dcc.network, dcc.sender, dcc.file, false);
}
