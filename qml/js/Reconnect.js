/****************************************************************************
**  Reconnect.js
**
**  Copyright information
**
**      Copyright (C) 2014-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

/**
 * @class   Reconnect
 * @author  Arttu Liimola
 * @version 2015-02-08
 *
 * Javascript to handle reconnect.
 */

var reconnectTime;
var countdown;
var dialogVisible;

/**
 * Initialize.
 *
 * @memberof Reconnect
 */
function init ()
{
    dialogVisible = false;
    reconnectTime = settings.getInt("Login", "Reconnect");
}

/**
 * Starts reconnecting.
 *
 * @memberof Reconnect
 */
function start ()
{
    countdown = reconnectTime;

    messageTimer.start();
}

/**
 * Called when the visible state of the reconnect dialog has changed.
 *
 * @memberof Reconnect
 * @param boolean visible Visible state of the reconnect dialog.
 */
function visibleChanged (visible)
{
    dialogVisible = visible;
}

/**
 * Updates the reconnect countdown.
 *
 * @memberof Reconnect
 */
function updateCountdown ()
{
    if ( countdown === 0 )
    {
        messageTimer.stop();

        if ( dialogVisible )
        {
            messageText.text = qsTr("Reconnecting to server!");
        }

        reconnect();
    }
    else
    {
        if ( dialogVisible )
        {
            messageText.text = qsTr("Reconnecting to server in " + countdown + " seconds!");
        }

        countdown--;
    }
}

/**
 * Called when client has connected to server.
 *
 * @memberof Reconnect
 */
function connected ()
{
    if ( dialogVisible )
    {
        messageText.text = qsTr("Connected. Login on the server!");
    }
}

/**
 * Called when client was unable to connect to server.
 *
 * @memberof Reconnect
 */
function unableToConnect ()
{
    countdown = reconnectTime;

    messageTimer.start();
}

/**
 * Called when client receives login result.
 *
 * @memberof Reconnect
 * @param string result Login result.
 */
function loginResult (result)
{
    messageText.text = "";

    switch ( result )
    {
        case "noError":
        {
            logged();

            break;
        }

        case "wrongUserPass":
        {
            cancel();

            break;
        }

        case "maxClientLimit":
        {
            cancel();

            break;
        }
    }
}

/**
 * Cancels reconnecting.
 *
 * @memberof Reconnect
 */
function cancel ()
{
    messageTimer.stop();

    cancelReconnecting();
}
