/****************************************************************************
**  ChatBox.js
**
**  Copyright information
**
**      Copyright (C) 2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

/**
 * @class   ChatBox
 * @author  Arttu Liimola
 * @version 2014-11-14
 *
 * Javascript to display chat text.
 */

var networks       = {};
var currentNetwork = null;
var currentChat    = null;
var scrollback     = 0;
var font           = null;
var sType          = "Scrollback";
var sSetting       = "Scrollback";
var cType          = "Colors";
var cSetting       = "Background";
var fType          = "Font";
var fSetting       = "ChatBox";

/**
 * Called when Client has logged in.
 *
 * @memberof ChatBox
 */
function logged ()
{
    chatBoxListViewContainer.color = settings.getString(cType, cSetting);

    scrollback = settings.getInt(sType, sSetting);
    font       = settings.getFont(fType, fSetting);

    createNoneNetwork();
}

/**
 * Clear.
 *
 * @memberof ChatBox
 */
function clear ()
{
    var networkA = new Array();

    for ( var network in networks )
    {
        networkA.push(network);
    }

    networkA.reverse();

    for ( var i in networkA )
    {
        removeNetwork(networkA[i]);
    }

    networks       = {};
    currentNetwork = null;
    currentChat    = null;

    createNoneNetwork();
}

/**
 * Create none network.
 *
 * @memberof ChatBox
 */
function createNoneNetwork ()
{
    insertNetwork("(none)");
    insertChat("(none)", "(none)");
}

/**
 * Inserts network.
 *
 * @memberof ChatBox
 * @param string network Network name.
 */
function insertNetwork (network)
{
    if ( currentChat )
    {
        var currentChatData = networks[currentNetwork][currentChat];

        currentChatData["view"].visible = false;
    }

    var chat         = "(server)";
    var component    = Qt.createComponent("../widgets/ChatBoxListView.qml");
    var view         = component.createObject(chatBoxListViewContainer);
    var model        = Qt.createQmlObject("import QtQuick 2.1; ListModel {}", view);
    var networkChats = {};

    networkChats[chat] = {"view": view, "lineBuffer": [""], "lineIndex": 0, "opened": false};
    networks[network]  = networkChats;
    currentNetwork     = network;
    currentChat        = chat;
    view.model         = model;
    view.font = font;

    setView(view);
}

/**
 * Inserts chat.
 *
 * @memberof ChatBox
 * @param string network Network name.
 * @param string chat    Chat name.
 */
function insertChat (network, chat)
{
    var currentChatData = networks[currentNetwork][currentChat];

    currentChatData["view"].visible = false;

    var component    = Qt.createComponent("../widgets/ChatBoxListView.qml");
    var view         = component.createObject(chatBoxListViewContainer);
    var model        = Qt.createQmlObject("import QtQuick 2.1; ListModel {}", view);
    var networkChats = networks[network];

    networkChats[chat] = {"view": view, "lineBuffer": [""], "lineIndex": 0, "opened": false};
    currentNetwork     = network;
    currentChat        = chat;
    view.model         = model;
    view.font          = font;

    setView(view);
}

/**
 * Removes network.
 *
 * @memberof ChatBox
 * @param string network Network name.
 */
function removeNetwork (network)
{
    var networkChats = networks[network];
    var chatData;

    for ( var i in networkChats )
    {
        chatData = networkChats[i];

        chatData.view.model.destroy();
        chatData.view.destroy();
    }

    delete networks[network];

    if ( network == currentNetwork )
    {
        currentNetwork = null;
        currentChat    = null;

        setView(null);

        if ( json.size(networks) === 1 )
        {
            setChat("(none)", "(none)");
        }
    }
}

/**
 * Removes chat.
 *
 * @memberof ChatBox
 * @param string network Network name.
 * @param string chat    Chat name.
 */
function removeChat (network, chat)
{
    var networkChats = networks[network];
    var chatData     = networkChats[chat];

    chatData.view.model.destroy();
    chatData.view.destroy();

    delete networkChats[chat];

    if ( network == currentNetwork && chat == currentChat )
    {
        currentNetwork = null;
        currentChat    = null;

        setView(null);
    }
}

/**
 * Renames chat.
 *
 * @memberof ChatBox
 * @param string network Network name.
 * @param string oldChat Old chat name.
 * @param string newChat New chat name.
 */
function renameChat (network, oldChat, newChat)
{
    var networkChats = networks[network];
    var chatData     = networkChats[oldChat];

    delete networkChats[oldChat];

    networkChats[newChat] = chatData;

    if ( currentNetwork === network && currentChat === oldChat )
    {
        currentChat = newChat;
    }
}

/**
 * Handles new message.
 *
 * @memberof ChatBox
 * @param string network   Network name.
 * @param string chat      Chat name.
 * @param string timestamp Timestamp.
 * @param string nick      Nick.
 * @param string msg       Message.
 */
function newMsg (network, chat, timestamp, nick, msg)
{
    var networkChats = networks[network];
    var view         = networkChats[chat]["view"];

    var atEnd = view.atYEnd;

    view.model.append({"timestamp": timestamp, "nick": nick, "msg": msg});

    if ( view.model.count > scrollback )
    {
        view.model.remove(0);
    }

    if ( atEnd )
    {
        view.positionViewAtEnd();
    }
}

/**
 * Sets current chat.
 *
 * @memberof ChatBox
 * @param string network Network name.
 * @param string chat    Chat name.
 */
function setChat (network, chat)
{
    if ( currentNetwork && currentChat )
    {
        var currentChatData = networks[currentNetwork][currentChat];
        var lineBuffer      = currentChatData["lineBuffer"];
        var lineIndex       = currentChatData["lineIndex"];

        currentChatData["view"].visible = false;
        lineBuffer[lineIndex]           = chatBoxLineView.text;
    }

    var chatData = networks[network][chat];
    var line     = chatData["lineBuffer"][chatData["lineIndex"]];
    var view     = chatData["view"];

    view.visible         = true;
    currentNetwork       = network;
    currentChat          = chat;
    chatBoxLineView.text = line;

    if ( !chatData["opened"] )
    {
        chatData["opened"] = true;

        view.positionViewAtEnd();
    }

    setView(view);
}

/**
 * Called when user presses up arrow key.
 *
 * @memberof ChatBox
 */
function lineBufferUp ()
{
    var limit        = 20;
    var networkChats = networks[currentNetwork];
    var chat         = networkChats[currentChat];
    var lineBuffer   = chat["lineBuffer"];
    var lineIndex    = chat["lineIndex"];
    var count        = lineBuffer.length;
    var line         = chatBoxLineView.text;
    var lastI        = count - 1;
    var prevI        = lineIndex - 1;

    if ( prevI > -1 )
    {
        if ( lineIndex === lastI )
        {
            if ( line.length > 0 )
            {
                lineBuffer[lastI] = line;
                lineBuffer.push("");

                if ( lastI == limit )
                {
                    lineBuffer.shift();
                }
            }
        }

        chatBoxLineView.text = lineBuffer[prevI];
        lineIndex = prevI;
    }

    chat["lineIndex"] = lineIndex;
}

/**
 * Called when user presses down arrow key.
 *
 * @memberof ChatBox
 */
function lineBufferDown ()
{
    var networkChats = networks[currentNetwork];
    var chat         = networkChats[currentChat];
    var lineBuffer   = chat["lineBuffer"];
    var lineIndex    = chat["lineIndex"];
    var count        = lineBuffer.length;
    var nextI        = lineIndex + 1;

    if ( nextI < count )
    {
        chatBoxLineView.text = lineBuffer[nextI];
        lineIndex            = nextI;
    }

    chat["lineIndex"] = lineIndex;
}

/**
 * Called when user presses return key.
 *
 * @memberof ChatBox
 */
function writeLine ()
{
    var limit        = 20;
    var networkChats = networks[currentNetwork];
    var chat         = networkChats[currentChat];
    var lineBuffer   = chat["lineBuffer"];
    var lineIndex    = chat["lineIndex"];
    var count        = lineBuffer.length;
    var lastI        = count - 1
    var line         = chatBoxLineView.text;

    if ( line.length > 0 )
    {
        if ( line !== lineBuffer[lastI - 1] )
        {
            lineBuffer[lastI] = line;
            lineBuffer.push('');

            if ( lastI == limit )
            {
                lineBuffer.shift();

                lineIndex = lastI;
            }
            else
            {
                lineIndex = count;
            }
        }
        else
        {
            lineIndex = lastI;
        }

        write(currentNetwork, currentChat, line);
    }

    chat["lineIndex"]    = lineIndex;
    chatBoxLineView.text = "";
}

/**
 * Called when user presses tab key.
 *
 * @memberof ChatBox
 */
function completeLine ()
{
    var line = chatBoxLineView.text;
    var pos  = chatBoxLineView.cursorPosition;

    complete(currentNetwork, currentChat, line, pos);
}

/**
 * Handles completed line.
 *
 * @memberof ChatBox
 * @param string network Network name.
 * @param string chat    Chat name.
 * @param string line    Completed line.
 */
function lineCompleted (network, chat, line)
{
    if ( currentNetwork === network && currentChat === chat )
    {
        chatBoxLineView.text = line;
    }
}

/**
 * Called when integer setting has changed.
 *
 * @memberof ChatBox
 * @param string  type    Setting type.
 * @param string  setting Setting name.
 * @param integer value   Setting value.
 */
function intSettingChanged (type, setting, value)
{
    if ( type === sType && setting === sSetting )
    {
        scrollback = value;

        var view;
        var count;
        var remove;

        for ( var network in networks )
        {
            var networkChats = networks[network];

            for ( var chat in networkChats )
            {
                view  = networkChats[chat]["view"];
                count = view.model.count;

                if ( count > scrollback )
                {
                    remove = count - scrollback;

                    for ( ; remove > 0; remove-- )
                    {
                        view.model.remove(0);
                    }
                }
            }
        }
    }
}

/**
 * Called when font setting has changed.
 *
 * @memberof ChatBox
 * @param string type    Setting type.
 * @param string setting Setting name.
 * @param font   value   Setting value.
 */
function fontSettingChanged (type, setting, value)
{
    if ( type === fType && setting === fSetting )
    {
        font = value;

        for ( var network in networks )
        {
            var networkChats = networks[network];

            for ( var chat in networkChats )
            {
                networkChats[chat].view.font = font;
            }
        }
    }
}

/**
 * Called when string setting has changed.
 *
 * @memberof ChatBox
 * @param string type    Setting type.
 * @param string setting Setting name.
 * @param string value   Setting value.
 */
function stringSettingChanged (type, setting, value)
{
    if ( type === cType && setting === cSetting )
    {
        chatBoxListViewContainer.color = value;
    }
}
