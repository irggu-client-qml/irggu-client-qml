/****************************************************************************
**  Options.js
**
**  Copyright information
**
**      Copyright (C) 2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

/**
 * @class   Options
 * @author  Arttu Liimola
 * @version 2014-06-15
 *
 * Javascript to handle options.
 */

var tabs   = new Array();
var colors = {};
var font   = null;

setSettings();

/**
 * Set setting variables.
 *
 * @memberof Options
 */
function setSettings ()
{
    colors["local"]     = new Array();
    colors["mirc"]      = new Array();
    colors["interface"] = new Array();

    var local = colors["local"];
    var mirc  = colors["mirc"];
    var iface = colors["interface"];

    local.push({"settings": "Local0", "server": "<local_0>"});
    local.push({"settings": "Local1", "server": "<local_1>"});
    local.push({"settings": "Local2", "server": "<local_2>"});
    local.push({"settings": "Local3", "server": "<local_3>"});
    local.push({"settings": "Local4", "server": "<local_4>"});
    local.push({"settings": "Local5", "server": "<local_5>"});
    local.push({"settings": "Local6", "server": "<local_6>"});
    local.push({"settings": "Local7", "server": "<local_7>"});
    local.push({"settings": "Local8", "server": "<local_8>"});
    local.push({"settings": "Local9", "server": "<local_9>"});
    local.push({"settings": "Local10", "server": "<local_10>"});
    local.push({"settings": "Local11", "server": "<local_11>"});
    local.push({"settings": "Local12", "server": "<local_12>"});
    local.push({"settings": "Local13", "server": "<local_13>"});
    local.push({"settings": "Local14", "server": "<local_14>"});
    local.push({"settings": "Local15", "server": "<local_15>"});

    mirc.push({"settings": "Mirc0", "server": "<mirc_0>"});
    mirc.push({"settings": "Mirc1", "server": "<mirc_1>"});
    mirc.push({"settings": "Mirc2", "server": "<mirc_2>"});
    mirc.push({"settings": "Mirc3", "server": "<mirc_3>"});
    mirc.push({"settings": "Mirc4", "server": "<mirc_4>"});
    mirc.push({"settings": "Mirc5", "server": "<mirc_5>"});
    mirc.push({"settings": "Mirc6", "server": "<mirc_6>"});
    mirc.push({"settings": "Mirc7", "server": "<mirc_7>"});
    mirc.push({"settings": "Mirc8", "server": "<mirc_8>"});
    mirc.push({"settings": "Mirc9", "server": "<mirc_9>"});
    mirc.push({"settings": "Mirc10", "server": "<mirc_10>"});
    mirc.push({"settings": "Mirc11", "server": "<mirc_11>"});
    mirc.push({"settings": "Mirc12", "server": "<mirc_12>"});
    mirc.push({"settings": "Mirc13", "server": "<mirc_13>"});
    mirc.push({"settings": "Mirc14", "server": "<mirc_14>"});
    mirc.push({"settings": "Mirc15", "server": "<mirc_15>"});

    iface.push({"settings": "Foreground", "server": "<foreground>"});
    iface.push({"settings": "Background"});
    iface.push({"settings": "ChatlistNewMessage"});
    iface.push({"settings": "ChatlistHighlight"});
    iface.push({"settings": "ChatlistPrivate"});
}

/**
 * Initialize.
 *
 * @memberof Options
 */
function init ()
{
    tabView.addTab("Appearance");
    tabView.addTab("Behaviour");
    tabView.addTab("Misc");

    tabs.push(appearanceTab);
    tabs.push(behaviourTab);
    tabs.push(miscTab);

    tabSelected(0);
}

/**
 * Called when Client has logged in.
 *
 * @memberof Options
 */
function logged ()
{
    var local = colors["local"];
    var mirc  = colors["mirc"];
    var iface = colors["interface"];

    var color;

    for ( var i in local )
    {
        color = local[i];

        color["color"] = settings.getString("Colors", color["settings"]);

        color["style"](color["color"]);

        writeSetting("colors", color["server"], color["color"]);
    }

    for ( var i in mirc )
    {
        color = mirc[i];

        color["color"] = settings.getString("Colors", color["settings"]);

        color["style"](color["color"]);

        writeSetting("colors", color["server"], color["color"]);
    }

    for ( var i in iface )
    {
        color = iface[i];

        color["color"] = settings.getString("Colors", color["settings"]);

        color["style"](color["color"]);
    }

    font = settings.getFont("Font", "ChatBox");

    fontButton.text = font.family + " " + font.pointSize;
    fontDialog.font = font;

    showSystrayIcon.checked       = settings.getBool("Systray", "ShowSystrayIcon");
    minimizeToSystray.checked     = settings.getBool("Systray", "MinimizeToSystray");
    closeToSystray.checked        = settings.getBool("Systray", "CloseToSystray");
    selectTimestamp.checked       = settings.getBool("Selecting", "SelectTimestamp");
    alertWindowActive.checked     = settings.getBool("Alert", "WindowActive");
    alertWindowDeactive.checked   = settings.getBool("Alert", "WindowDeactive");
    alertWindowClosed.checked     = settings.getBool("Alert", "WindowClosed");
    alertShowNotification.checked = settings.getBool("Alert", "ShowNotification");
    alertBlinkSystray.checked     = settings.getBool("Alert", "BlinkSystray");
    timestampEnabled.checked      = settings.getBool("Timestamp", "Enabled");
    timestampFormat.text          = settings.getString("Timestamp", "Format");
    priority.currentIndex         = settings.getInt("Priority", "Priority");
    scrollback.text               = settings.getInt("Scrollback", "Scrollback");

    writeSetting("timestamp", "format", timestampFormat.text);
}

/**
 * Called when options dialog's tab has been selected.
 *
 * @memberof Options
 * @param integer index Index of options dialog's tab.
 */
function tabSelected (index)
{
    hideTabs();

    tabs[index].visible = true;
}

/**
 * Opens color dialog.
 *
 * @memberof Options
 * @param string  category Color category.
 * @param integer index    Color number.
 */
function openColorDialog (category, index)
{
    var color = colors[category][index];

    colorDialog.category = category;
    colorDialog.index    = index;
    colorDialog.color    = color["color"];

    colorDialog.open();
}

/**
 * Called when color dialog has been closed.
 *
 * @memberof Options
 */
function colorDialogClosed ()
{
    var color = colors[colorDialog.category][colorDialog.index];

    color["color"] = colorDialog.color.toString();

    color["style"](color["color"]);

    settings.setString("Colors", color["settings"], color["color"]);

    if ( color.hasOwnProperty("server") )
    {
        writeSetting("colors", color["server"], color["color"]);
    }
}

/**
 * Opens font dialog.
 *
 * @memberof Options
 */
function openFontDialog ()
{
    fontDialog.open();
}

/**
 * Called when font dialog has been closed.
 *
 * @memberof Options
 */
function fontDialogClosed ()
{
    font = fontDialog.font;

    fontButton.text = font.family + " " + font.pointSize;

    settings.setFont("Font", "ChatBox", font);
}

/**
 * Called when show systray icon checkbox has been clicked.
 *
 * @memberof Options
 */
function showSystrayChanged ()
{
    if ( showSystrayIcon.checked )
    {
        minimizeToSystray.enabled = true;
        closeToSystray.enabled    = true;
    }
    else
    {
        minimizeToSystray.checked = false;
        minimizeToSystray.enabled = false;

        closeToSystray.checked = false;
        closeToSystray.enabled = false;
    }

    settings.setBool("Systray", "ShowSystrayIcon", showSystrayIcon.checked);
}

/**
 * Called when minimize to systray checkbox has been clicked.
 *
 * @memberof Options
 */
function minimizeToSystrayChanged ()
{
    settings.setBool("Systray", "MinimizeToSystray", minimizeToSystray.checked);
}

/**
 * Called when close to systray checkbox has been clicked.
 *
 * @memberof Options
 */
function closeToSystrayChanged ()
{
    settings.setBool("Systray", "CloseToSystray", closeToSystray.checked);
}

/**
 * Called when select timestamp checkbox has been clicked.
 *
 * @memberof Options
 */
function selectTimestampChanged ()
{
    settings.setBool("Selecting", "SelectTimestamp", selectTimestamp.checked);
}

/**
 * Called when alert when window is active checkbox has been clicked.
 *
 * @memberof Options
 */
function alertWindowActiveChanged ()
{
    settings.setBool("Alert", "WindowActive", alertWindowActive.checked);
}

/**
 * Called when alert when window is deactive checkbox has been clicked.
 *
 * @memberof Options
 */
function alertWindowDeactiveChanged ()
{
    settings.setBool("Alert", "WindowDeactive", alertWindowDeactive.checked);
}

/**
 * Called when alert when window is closed checkbox has been clicked.
 *
 * @memberof Options
 */
function alertWindowClosedChanged ()
{
    settings.setBool("Alert", "WindowClosed", alertWindowClosed.checked);
}

/**
 * Called when show notification checkbox has been clicked.
 *
 * @memberof Options
 */
function alertShowNotificationChanged ()
{
    settings.setBool("Alert", "ShowNotification", alertShowNotification.checked);
}

/**
 * Called when blink systray checkbox has been clicked.
 *
 * @memberof Options
 */
function alertBlinkSystrayChanged ()
{
    settings.setBool("Alert", "BlinkSystray", alertBlinkSystray.checked);
}

/**
 * Called when enable timestamp checkbox has been clicked.
 *
 * @memberof Options
 */
function timestampEnabledChanged ()
{
    var checked = timestampEnabled.checked;

    settings.setBool("Timestamp", "Enabled", checked);

    writeSetting("timestamp", "state", ( checked ? "enabled" : "disabled" ));
}

/**
 * Called when priority combo box's value has changed.
 *
 * @memberof Options
 * @param integer index Index of priority combo box.
 */
function priorityChanged (index)
{
    settings.setInt("Priority", "Priority", index);
}

/**
 * Closes options dialog.
 *
 * @memberof Options
 */
function close ()
{
    if ( settings.getString("Timestamp", "Format") != timestampFormat.text )
    {
        settings.setString("Timestamp", "Format", timestampFormat.text);

        writeSetting("timestamp", "format", timestampFormat.text);
    }

    var scrollbackInt = parseInt(scrollback.text);

    if ( scrollbackInt )
    {
        scrollback.text = scrollbackInt;

        settings.setInt("Scrollback", "Scrollback", scrollbackInt);
    }
    else
    {
        scrollback.text = settings.getInt("Scrollback", "Scrollback");
    }

    settings.writeUserSettings();

    optionsDialog.close();
}

/**
 * Inserts options dialog's color button style to colors json object.
 *
 * @memberof Options
 * @param string       category Color category.
 * @param integer      index    Color number.
 * @param void(string) callback Callback to set color to button.
 */
function insertStyle (category, index, callback)
{
    var color = colors[category][index];

    color["style"] = callback;
}

/**
 * Hides options dialogs tabs.
 *
 * @memberof Options
 */
function hideTabs ()
{
    for ( var i in tabs )
    {
        tabs[i].visible = false;
    }
}
