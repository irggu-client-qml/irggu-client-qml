/****************************************************************************
**  IrGGu.js
**
**  Copyright information
**
**      Copyright (C) 2013-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

/**
 * @class   IrGGu
 * @author  Arttu Liimola
 * @version 2015-02-07
 *
 * Main javascript.
 */

var networks       = {};
var currentNetwork = null;
var currentChat    = null;
var login          = null;
var networkList    = null;
var chatBox        = null;
var nameList       = null;
var options        = null;
var dcc            = null;
var reconnect      = null;
var setPos         = false;
var loggedServer   = false;
var quitClient     = false;
var idle           = false;
var windowVisible  = false;
var reconnecting   = false;
var x              = 0;
var y              = 0;

/**
 * Initialize.
 *
 * @memberof IrGGu
 */
function init ()
{
    login       = loginDialog.js();
    networkList = networkListView.js();
    chatBox     = chatBoxView.js();
    nameList    = nameListView.js();
    options     = optionsDialog.js();
    dcc         = dccDialog.js();
    reconnect   = reconnectDialog.js();
}

/**
 * Clear data.
 *
 * @memberof IrGGu
 */
function clear ()
{
    networkList.clear();
    chatBox.clear();
    nameList.clear();
    dcc.clear();
}

/**
 * Sets clients idle state.
 *
 * @memberof IrGGu
 */
function setIdleState ()
{
    var windowClosed   = settings.getBool("Alert", "WindowClosed");
    var windowDeactive = settings.getBool("Alert", "WindowDeactive");
    var windowActive   = settings.getBool("Alert", "WindowActive");
    var alerts         = windowClosed | windowDeactive | windowActive;
    var priority       = settings.getInt("Priority", "Priority");

    setIdle(idle, alerts, priority);
}

/**
 * Logout.
 *
 * @memberof IrGGu
 */
function logout ()
{
    reconnectDialog.close();
    mainWindow.close();

    clear();

    systray.setVisible(false);

    loggedServer = false;

    login.startLogout();
    loginDialog.show();
}

/**
 * Quit.
 *
 * @memberof IrGGu
 */
function quit ()
{
    mainWindow.close();

    loggedServer = false;
    quitClient   = true;

    login.startLogout();
}

/**
 * Called when the active state of the window has changed.
 *
 * @memberof IrGGu
 */
function activeChanged ()
{
    if ( mainWindow.active && setPos )
    {
        setPos = false;

        mainWindow.setX(x);
        mainWindow.setY(y);
    }

    notify.setWindowActive(mainWindow.active);
}

/**
 * Called when the visible state of the main window has changed.
 *
 * @memberof IrGGu
 * @param boolean visible Visible state of the main window.
 */
function visibleChanged (visible)
{
    windowVisible = visible;

    if ( windowVisible && reconnecting)
    {
        reconnectDialog.show();
    }
    else if ( reconnecting )
    {
        reconnectDialog.hide();
    }

    notify.setWindowClosed(visible)
}

/**
 * Called when the active state of the screensaver has changed.
 *
 * @memberof IrGGu
 * @param boolean active Active state of the screensaver.
 */
function screensaverStateChanged (active)
{
    notify.setScreensaverActive(active);

    idle = active;

    setIdleState();
}

/**
 * Called when systray icon has been clicked.
 *
 * @memberof IrGGu
 */
function systrayClicked ()
{
    if ( mainWindow.visible )
    {
        x = mainWindow.x;
        y = mainWindow.y;

        mainWindow.hide();
    }
    else
    {
        setPos = true;

        mainWindow.show();
    }
}

/**
 * Called when entry in systray menu has been clicked.
 *
 * @memberof IrGGu
 * @param string action Entry in systray menu.
 */
function systrayMenuClicked (action)
{
    switch ( action )
    {
        case "Show/Hide":
        {
            systrayClicked();

            break;
        }

        case "Logout":
        {
            logout();

            break;
        }

        case "Quit":
        {
            quit();

            break;
        }
    }
}

/**
 * Called when session is ready for login.
 *
 * @memberof IrGGu
 */
function sessionReady ()
{
    loginDialog.show();

    login.sessionReady();
}

/**
 * Called when client has connected to server.
 *
 * @memberof IrGGu
 */
function connected ()
{
    if ( !reconnecting )
    {
        login.connected();
    }
    else
    {
        reconnect.connected();
    }

    if ( idle )
    {
        setIdleState();
    }
}

/**
 * Called when client has disconnected from server.
 *
 * @memberof IrGGu
 */
function disconnected ()
{
    if ( loggedServer )
    {
        reconnect.start();

        reconnecting = true;

        if ( windowVisible )
        {
            reconnectDialog.show();
        }
    }

    loggedServer = false;

    if ( quitClient )
    {
        Qt.quit();
    }
}

/**
 * Called when client was unable to connect to server.
 *
 * @memberof IrGGu
 */
function unableToConnect ()
{
    if ( !reconnecting )
    {
        login.unableToConnect();
    }
    else
    {
        reconnect.unableToConnect();
    }
}

/**
 * Called when client receives login result.
 *
 * @memberof IrGGu
 * @param string result Login result.
 */
function loginResult (result)
{
    if ( !reconnecting )
    {
        login.loginResult(result);
    }
    else
    {
        reconnect.loginResult(result);
    }
}

/**
 * Called when Client has logged in.
 *
 * @memberof IrGGu
 */
function logged ()
{
    loginDialog.close();
    reconnectDialog.close();

    if ( settings.getBool("Systray", "ShowSystrayIcon") )
    {
        systray.setVisible(true);
    }

    if ( !reconnecting )
    {
        chatBox.logged();
        networkList.logged();
        options.logged();

        mainWindow.showMaximized();
    }

    readyForData(reconnecting);

    setPos       = true;
    loggedServer = true;
    reconnecting = false;
}

/**
 * Inserts network.
 *
 * @memberof IrGGu
 * @param string network Network name.
 */
function insertNetwork (network)
{
    saveNameListState();

    var chat         = "(server)";
    var networkChats = {};
    var chatData     = {"visible": false, "width": 0};

    networkChats[chat] = chatData;
    networks[network]  = networkChats;
    currentNetwork     = network;
    currentChat        = chat;

    loadNamesListState();

    networkList.insertNetwork(network);
    chatBox.insertNetwork(network);
    nameList.insertNetwork(network);
}

/**
 * Inserts chat.
 *
 * @memberof IrGGu
 * @param string network Network name.
 * @param string chat    Chat name.
 */
function insertChat (network, chat)
{
    saveNameListState();

    var visible = true;
    var width   = 150;

    var fChar = chat.charAt(0);

    if ( fChar !== '&' && fChar !== '#' && fChar !== '+' && fChar !== '!' )
    {
        visible = false;
        width   = 0;
    }

    var networkChats = networks[network];
    var chatData     = {"visible": visible, "width": width};

    networkChats[chat] = chatData;
    currentNetwork     = network;
    currentChat        = chat;

    loadNamesListState();

    networkList.insertChat(network, chat);
    chatBox.insertChat(network, chat);
    nameList.insertChat(network, chat);
}

/**
 * Inserts nick.
 *
 * @memberof IrGGu
 * @param string network Network name.
 * @param string chat    Chat name.
 * @param string nick    Nick.
 * @param int    index   Index of nick.
 */
function insertNick (network, chat, nick, index)
{
    nameList.insertNick(network, chat, nick, index);
}

/**
 * Inserts nicks.
 *
 * @memberof IrGGu
 * @param string network Network name.
 * @param string chat    Chat name.
 * @param array  nicks   Nicks.
 */
function insertNicks (network, chat, nicks)
{
    nameList.insertNicks(network, chat, nicks);
}

/**
 * Removes network.
 *
 * @memberof IrGGu
 * @param string network Network name.
 */
function removeNetwork (network)
{
    if ( networks.hasOwnProperty(network) )
    {
        currentNetwork = null;
        currentChat    = null;

        delete networks[network];

        chatBox.removeNetwork(network);
        nameList.removeNetwork(network);
        networkList.removeNetwork(network);
    }
}

/**
 * Removes chat.
 *
 * @memberof IrGGu
 * @param string network Network name.
 * @param string chat    Chat name.
 */
function removeChat (network, chat)
{
    if ( networks.hasOwnProperty(network) )
    {
        var networkChats = networks[network];

        if ( networkChats.hasOwnProperty(chat) )
        {
            currentNetwork = null;
            currentChat    = null;

            delete networkChats[chat];

            chatBox.removeChat(network, chat);
            nameList.removeChat(network, chat);
            networkList.removeChat(network, chat);
        }
    }
}

/**
 * Removes nick.
 *
 * @memberof IrGGu
 * @param string network Network name.
 * @param string chat    Chat name.
 * @param int    index   Index of nick.
 */
function removeNick (network, chat, index)
{
    nameList.removeNick(network, chat, index);
}

/**
 * Renames chat.
 *
 * @memberof IrGGu
 * @param string network Network name.
 * @param string oldChat Old chat name.
 * @param string newChat New chat name.
 */
function renameChat (network, oldChat, newChat)
{
    if ( networks.hasOwnProperty(network) )
    {
        var networkChats = networks[network];

        if ( networkChats.hasOwnProperty(oldChat) )
        {
            var chatData = networkChats[oldChat];

            delete networkChats[oldChat];

            networkChats[newChat] = chatData;

            if ( currentNetwork === network && currentChat === oldChat )
            {
                currentChat = newChat;
            }

            networkList.renameChat(network, oldChat, newChat);
            chatBox.renameChat(network, oldChat, newChat);
            nameList.renameChat(network, oldChat, newChat);
        }
    }
}

/**
 * Handles new message.
 *
 * @memberof IrGGu
 * @param string network   Network name.
 * @param string chat      Chat name.
 * @param string timestamp Timestamp.
 * @param string nick      Nick.
 * @param string msg       Message.
 * @param string type      Message type.
 */
function newMsg (network, chat, timestamp, nick, msg, type)
{
    network = network === "(current)" ? currentNetwork : network;
    chat    = chat === "(current)" ? currentChat : chat;

    networkList.newMsg(network, chat, type);
    chatBox.newMsg(network, chat, timestamp, nick, msg);

    var current = network === currentNetwork && chat === currentChat;

    switch ( type )
    {
        case "normal":
        {
            var fChar = chat.charAt(0);

            if ( fChar !== '&' && fChar !== '#' && fChar !== '+' && fChar !== '!'
                 && fChar !== '(' )
            {
                notify.notify(network, chat, "Private", current);
            }

            break;
        }

        case "highlight":
        {
            notify.notify(network, chat, "Highlight", current);

            break;
        }
    }
}

/**
 * Called when client receives new DCC file command.
 *
 * @memberof IrGGu
 * @param string network Network name.
 * @param string sender  Sender's name.
 * @param string file    Filename.
 */
function newDCCFile (network, sender, file)
{
    dcc.newDCCFile(network, sender, file);
}

/**
 * Called when client receives close DCC file command.
 *
 * @memberof IrGGu
 * @param string network Network name.
 * @param string sender  Sender's name.
 * @param string file    Filename.
 */
function closeDCCFile (network, sender, file)
{
    dcc.closeDCCFile(network, sender, file);
}

/**
 * Called when client receives line completed command.
 *
 * @memberof IrGGu
 * @param string network Network name.
 * @param string chat    Chat name.
 * @param string line    Completed line.
 */
function lineCompleted (network, chat, line)
{
    chatBox.lineCompleted(network, chat, line);
}

/**
 * Called when client receives alert command.
 *
 * @memberof IrGGu
 * @param string network Network name.
 * @param string sender  Triggerer of alert.
 * @param string type    Alert type.
 */
function showAlert (network, sender, type)
{
    var current = network === currentNetwork && sender === currentChat;

    switch ( type )
    {
        case "private":
        {
            notify.notify(network, sender, "Private", current);

            break;
        }

        case "highlight":
        {
            notify.notify(network, sender, "Highlight", current);

            break;
        }

        case "dcc":
        {
            notify.notify(network, sender, "DCC", current);

            break;
        }
    }
}

/**
 * Called when client receives close alert command.
 *
 * @memberof IrGGu
 * @param string network Network name.
 * @param string sender  Triggerer of alert.
 * @param string type    Alert type.
 */
function closeAlert (network, sender, type)
{

}

/**
 * Called when chat has been selected on network list.
 *
 * @memberof IrGGu
 * @param string network Network name.
 * @param string chat    Chat name.
 */
function chatSelected (network, chat)
{
    saveNameListState();

    currentNetwork = network;
    currentChat    = chat;

    loadNamesListState();

    chatBox.setChat(network, chat);
    nameList.setChat(network, chat);
}

/**
 * Saves the state of name list's visibility and width for the current chat.
 *
 * @memberof IrGGu
 */
function saveNameListState ()
{
    if ( currentNetwork && currentChat )
    {
        var networkChats = networks[currentNetwork];
        var chatData     = networkChats[currentChat];

        chatData["visible"] = nameListView.visible;
        chatData["width"]   = nameListView.width;
    }
}

/**
 * Loads the state of name list's visibility and width for the current chat.
 *
 * @memberof IrGGu
 */
function loadNamesListState ()
{
    if ( currentNetwork && currentChat )
    {
        var networkChats = networks[currentNetwork];
        var chatData     = networkChats[currentChat];

        nameListView.visible = chatData["visible"];
        nameListView.width   = chatData["width"];
    }
}
