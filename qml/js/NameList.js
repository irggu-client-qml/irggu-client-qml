/****************************************************************************
**  NameList.js
**
**  Copyright information
**
**      Copyright (C) 2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

/**
 * @class   NameList
 * @author  Arttu Liimola
 * @version 2014-06-17
 *
 * Javascript to handle name list.
 */

var networks       = {};
var currentNetwork = null;
var currentChat    = null;

/**
 * Clears name list.
 *
 * @memberof NameList
 */
function clear ()
{
    nameListView.model = null;

    var networkA = new Array();

    for ( var network in networks )
    {
        networkA.push(network);
    }

    for ( var i in networkA )
    {
        removeNetwork(networkA[i]);
    }

    networks = {};
    currentNetwork = null;
    currentChat = null;
}

/**
 * Inserts network.
 *
 * @memberof NameList
 * @param string network Network name.
 */
function insertNetwork (network)
{
    var chat         = "(server)";
    var model        = null;
    var networkChats = {};

    networkChats[chat] = model;
    networks[network]  = networkChats;
    currentNetwork     = network;
    currentChat        = chat;
    nameListView.model = model;
}

/**
 * Inserts chat.
 *
 * @memberof NameList
 * @param string network Network name.
 * @param string chat    Chat name.
 */
function insertChat (network, chat)
{
    var model        = Qt.createQmlObject("import QtQuick 2.1; ListModel {}", nameListView);
    var networkChats = networks[network];

    networkChats[chat] = model;
    currentNetwork     = network;
    currentChat        = chat;
    nameListView.model = model;
}

/**
 * Inserts nick.
 *
 * @memberof NameList
 * @param string network Network name.
 * @param string chat    Chat name.
 * @param string nick    Nick.
 * @param int    index   Index of nick.
 */
function insertNick (network, chat, nick, index)
{
    var networkChats = networks[network];
    var model        = networkChats[chat];

    model.insert(index, {"name": nick, "hover": false});
}

/**
 * Inserts nicks.
 *
 * @memberof NameList
 * @param string network Network name.
 * @param string chat    Chat name.
 * @param array  nicks   Nicks.
 */
function insertNicks (network, chat, nicks)
{
    var networkChats = networks[network];
    var model        = networkChats[chat];
    var len          = nicks.length;

    model.clear();

    for ( var i = 0; i < len; i++ )
    {
        model.insert(i, {"name": nicks[i], "hover": false});
    }
}

/**
 * Removes network.
 *
 * @memberof NameList
 * @param string network Network name.
 */
function removeNetwork (network)
{
    var networkChats = networks[network];
    var chatData;

    for ( var chat in networkChats )
    {
        chatData = networkChats[chat];

        if ( chatData )
        {
            chatData.destroy();
        }
    }

    delete networks[network];
}

/**
 * Removes chat.
 *
 * @memberof NameList
 * @param string network Network name.
 * @param string chat    Chat name.
 */
function removeChat (network, chat)
{
    var networkChats = networks[network];
    var chatData     = networkChats[chat];

    if ( chatData )
    {
        chatData.destroy();
    }

    delete networkChats[chat];
}

/**
 * Removes nick.
 *
 * @memberof NameList
 * @param string network Network name.
 * @param string chat    Chat name.
 * @param int    index   Index of nick.
 */
function removeNick (network, chat, index)
{
    var networkChats = networks[network];
    var model        = networkChats[chat];

    model.remove(index);
}

/**
 * Renames chat.
 *
 * @memberof NameList
 * @param string network Network name.
 * @param string oldChat Old chat name.
 * @param string newChat New chat name.
 */
function renameChat (network, oldChat, newChat)
{
    var networkChats = networks[network];
    var model        = networkChats[oldChat];

    delete networkChats[oldChat];

    networkChats[newChat] =  model;

    if ( currentNetwork === network && currentChat === oldChat )
    {
        currentChat = newChat;
    }
}

/**
 * Called when mouse hovers on name.
 *
 * @memberof NameList
 * @param integer index Index of name list.
 */
function mouseOn (index)
{
    var networkChats = networks[currentNetwork];
    var model        = networkChats[currentChat];

    model.get(index).hover = true;
}

/**
 * Called when mouse does not hover name anymore.
 *
 * @memberof NameList
 * @param integer index Index of name list.
 */
function mouseOff (index)
{
    var networkChats = networks[currentNetwork];
    var model        = networkChats[currentChat];

    model.get(index).hover = false;
}

/**
 * Called when name has been clicked.
 *
 * @memberof NameList
 * @param integer index Index of name list.
 */
function clicked (index)
{
    var nick  = nameListView.model.get(index).name;
    var fChar = nick.charAt(0);

    if ( fChar ===  '@' || fChar ===  '%' || fChar ===  '+' )
    {
        nick = nick.slice(1);
    }

    query(currentNetwork, nick);
}

/**
 * Sets current chat.
 *
 * @memberof NameList
 * @param string network Network name.
 * @param string chat    Chat name.
 */
function setChat (network, chat)
{
    var networkChats = networks[network];
    var model        = networkChats[chat];

    currentNetwork     = network;
    currentChat        = chat;
    nameListView.model = model;
}
