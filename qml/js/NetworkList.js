/****************************************************************************
**  NetworkList.js
**
**  Copyright information
**
**      Copyright (C) 2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

/**
 * @class   NetworkList
 * @author  Arttu Liimola
 * @version 2014-11-14
 *
 * Javascript to handle network list.
 */

var networks        = new Array();
var chats           = {};
var chatsColor      = {};
var selectedNetwork = null;
var selectedChat    = null;
var defaultColor    = null;
var newMsgColor     = null;
var highlightColor  = null;
var privateMsgColor = null;
var cType           = "Colors";
var cNewMsg         = "ChatlistNewMessage";
var cHighlight      = "ChatlistHighlight";
var cPrivateMsg     = "ChatlistPrivate";
var menuName        = null;

/**
 * Initialize.
 *
 * @memberof NetworkList
 */
function init ()
{
    menuName         = networkListMenu.insertItem(0, "");
    menuName.enabled = false;

    networkListMenu.insertSeparator(1);
}

/**
 * Called when Client has logged in.
 *
 * @memberof NetworkList
 */
function logged ()
{
    defaultColor    = "#000000";
    newMsgColor     = settings.getString(cType, cNewMsg);
    highlightColor  = settings.getString(cType, cHighlight);
    privateMsgColor = settings.getString(cType, cPrivateMsg);
}

/**
 * Clears network list.
 *
 * @memberof NetworkList
 */
function clear ()
{
    networks        = new Array();
    chats           = {};
    chatsColor      = {};
    selectedNetwork = null;
    selectedChat    = null;

    networkListModel.clear();
}

/**
 * Inserts network.
 *
 * @memberof NetworkList
 * @param string network Network name.
 */
function insertNetwork (network)
{
    unsetSelected();

    networks.push(network);
    networks.sort(array.sort);

    var i     = networks.indexOf(network);
    var count = networks.length;
    var lastI = count - 1;
    var index = -1;

    if ( count > 1 )
    {
        if ( i != lastI )
        {
            index = networkIndex(networks[i + 1]);
        }
    }

    if ( index > -1 )
    {
        networkListModel.insert(index, { "colorCode":defaultColor, "name":network, "server": true,
                                         "selected": true, "hover": false, "chats": false });
    }
    else
    {
        networkListModel.append({ "colorCode":defaultColor, "name":network, "server": true,
                                  "selected": true, "hover": false, "chats": false });
    }

    selectedNetwork = network;
    selectedChat    = "(server)";
}

/**
 * Inserts chat.
 *
 * @memberof NetworkList
 * @param string network Network name.
 * @param string chat    Chat name.
 */
function insertChat (network, chat)
{
    var networkChats;
    var index = networkIndex(network);

    if ( hasChats(network) )
    {
        networkChats = chats[network];

        if ( !networkListModel.get(index).chats )
        {
            toggleChats(index);
        }
    }
    else
    {
        networkChats = new Array();

        networkListModel.get(index).chats = true;
    }

    networkChats.push(chat);
    networkChats.sort(array.sort);

    index += networkChats.indexOf(chat) + 1;

    unsetSelected();

    networkListModel.insert(index, { "colorCode":defaultColor, "name":chat, "server": false,
                                     "selected": true, "hover": false, "network":network });

    chats[network]  = networkChats;
    selectedNetwork = network;
    selectedChat    = chat;
}

/**
 * Removes network.
 *
 * @memberof NetworkList
 * @param string network Network name.
 */
function removeNetwork (network)
{
    var i     = networks.indexOf(network);
    var index = networkIndex(network);
    var count = networks.length;
    var lastI = count - 1;

    if ( selectedNetwork === network && count > 1 )
    {
        var selectI = 0;

        if ( i !== lastI )
        {
            var nextN = networks[i + 1];
            var nextI = networkIndex(nextN);

            if ( hasChats(nextN) && networkListModel.get(nextI).chats )
            {
                nextI++;
            }

            selectI = nextI;
        }
        else
        {
            var prevN = networks[i - 1];
            var prevI = networkIndex(prevN);

            if ( hasChats(prevN) && networkListModel.get(prevI).chats )
            {
                prevI++;
            }

            selectI = prevI;
        }

        selectChat(selectI);
    }

    if ( chatsColor.hasOwnProperty(network) )
    {
        delete chatsColor[network];
    }

    if ( hasChats(network) )
    {
        if ( networkListModel.get(index).chats )
        {
            toggleChats(network);
        }

        delete chats[network];
    }

    networks.splice(i, 1);
    networkListModel.remove(index);
}

/**
 * Removes chat.
 *
 * @memberof NetworkList
 * @param string network Network name.
 * @param string chat    Chat name.
 */
function removeChat (network, chat)
{
    var networkChats = chats[network];
    var nIndex       = networkIndex(network);
    var cIndex       = chatIndex(network, chat);
    var i            = networkChats.indexOf(chat);
    var count        = networkChats.length;
    var lastI        = count - 1;

    if ( selectedNetwork === network && selectedChat === chat )
    {
        var selectI = nIndex;

        if ( networkListModel.get(nIndex).chats && count > 1 )
        {
            selectI = cIndex;

            if ( i !== lastI )
            {
                selectI++;
            }
            else
            {
                selectI--;
            }
        }

        selectChat(selectI);
    }

    if ( chatsColor.hasOwnProperty(network) )
    {
        var nColor = chatsColor[network];

        if ( nColor.hasOwnProperty(chat) )
        {
            delete nColor[chat];
        }
    }

    if ( networkListModel.get(nIndex).chats )
    {
        networkListModel.remove(cIndex);
    }

    networkChats.splice(i, 1);

    if ( count === 1 )
    {
        delete chats[network];

        networkListModel.get(nIndex).chats = false;
    }
}

/**
 * Renames chat.
 *
 * @memberof NetworkList
 * @param string network Network name.
 * @param string oldChat Old chat name.
 * @param string newChat New chat name.
 */
function renameChat (network, oldChat, newChat)
{
    var networkChats = chats[network];
    var oldI         = networkChats.indexOf(oldChat);
    var oldIndex     = chatIndex(network, oldChat);

    if ( chatsColor.hasOwnProperty(network) )
    {
        var nColor = chatsColor[network];

        if ( nColor.hasOwnProperty(oldChat) )
        {
            var color = nColor[oldChat];

            delete nColor[oldChat];

            nColor[newChat] = color;
        }
    }

    networkChats.splice(oldI, 1);
    networkChats.push(newChat);
    networkChats.sort(array.sort)

    if ( oldIndex > -1 )
    {
        var i     = networkChats.indexOf(newChat);
        var index = oldIndex - ( oldI - i );

        networkListModel.move(oldIndex, index, 1);

        networkListModel.get(index).name = newChat;
    }

    if ( selectedNetwork === network && selectedChat === oldChat )
    {
        selectedChat = newChat;
    }
}

/**
 * Handles new message.
 *
 * @memberof NetworkList
 * @param string network Network name.
 * @param string chat    Chat name.
 * @param string type    Message type.
 */
function newMsg (network, chat, type)
{
    var current = selectedNetwork === network && selectedChat === chat;

    if ( network !== '(none)' && !current && type !== "own" && type !== "append" )
    {
        var index = -1;
        var color = "";

        if ( chat !== "(server)" )
        {
            index = chatIndex(network, chat);
        }
        else
        {
            index = networkIndex(network);
        }

        switch ( type )
        {
            case "normal":
            {
                var fChar = chat.charAt(0);

                if ( fChar !== '&' && fChar !== '#' && fChar !== '+' && fChar !== '!'
                     && fChar !== '(' )
                {
                    color = privateMsgColor;
                }
                else
                {
                    color = newMsgColor;
                }

                break;
            }

            case "highlight":
            {
                color = highlightColor;

                break;
            }
        }

        if ( index > -1 )
        {
            networkListModel.get(index).colorCode = color;
        }

        chatsColor[network] = {};
        chatsColor[network][chat] = color;
    }
}

/**
 * Called when mouse hovers on chat.
 *
 * @memberof NetworkList
 * @param integer index Index of network list.
 */
function mouseOn (index)
{
    networkListModel.get(index).hover = true;
}

/**
 * Called when mouse does not hover chat anymore.
 *
 * @memberof NetworkList
 * @param integer index Index of network list.
 */
function mouseOff (index)
{
    networkListModel.get(index).hover = false;
}

/**
 * Called when Chat has been clicked.
 *
 * @memberof NetworkList
 * @param MouseEvent mouse Mouse event.
 * @param integer    index Index of network list.
 */
function clicked (mouse, index)
{
    switch ( mouse.button )
    {
        case Qt.LeftButton:
        {
            selectChat(index);

            break;
        }

        case Qt.RightButton:
        {
            networkListMenu.index = index;
            menuName.text         = networkListModel.get(index).name;

            networkListMenu.popup();

            break;
        }
    }
}

/**
 * Selects chat.
 *
 * @memberof NetworkList
 * @param integer index Index of network list.
 */
function selectChat (index)
{
    unsetSelected();

    networkListModel.get(index).selected  = true;
    networkListModel.get(index).colorCode = defaultColor;

    if ( networkListModel.get(index).server )
    {
        selectedNetwork = networkListModel.get(index).name;
        selectedChat    = "(server)";
    }
    else
    {
        selectedNetwork = networkListModel.get(index).network;
        selectedChat    = networkListModel.get(index).name;
    }

    if ( chatsColor.hasOwnProperty(selectedNetwork) )
    {
        var nColor = chatsColor[selectedNetwork];

        if ( nColor.hasOwnProperty(selectedChat) )
        {
            delete nColor[selectedChat];
        }
    }

    chatSelected(selectedNetwork, selectedChat);
}

/**
 * Toggles chats of a network.
 *
 * @memberof NetworkList
 * @param integer index Index of network list.
 */
function toggleChats (index)
{
    var network = networkListModel.get(index).name;

    if ( hasChats(network) )
    {
        var networkChats = chats[network];
        var chatsVisible = networkListModel.get(index).chats;

        networkListModel.get(index).chats = !chatsVisible;

        index++;

        if ( chatsVisible )
        {
            for ( var i = 0; i < networkChats.length; i++ )
            {
                networkListModel.remove(index);
            }
        }
        else
        {
            var j;
            var chat;
            var color;
            var nColor;
            var selected = false;

            for ( var i = 0; i < networkChats.length; i++ )
            {
                j        = index + i;
                chat     = networkChats[i];
                selected = network === selectedNetwork && chat === selectedChat;
                color    = defaultColor;

                if ( chatsColor.hasOwnProperty(network) )
                {
                    nColor = chatsColor[network];

                    if ( nColor.hasOwnProperty(chat) )
                    {
                        color = nColor[chat];
                    }
                }

                networkListModel.insert(j, { "colorCode":color, "name": chat,
                                             "server": false, "selected": selected, "hover": false,
                                             "network": network });
            }
        }
    }
}

/**
 * Closes network or chat.
 *
 * @memberof NetworkList
 * @param integer index Index of network list.
 */
function close (index)
{
    if ( networkListModel.get(index).server )
    {
        quitNetwork(networkListModel.get(index).name);
    }
    else
    {
        closeChat(networkListModel.get(index).network, networkListModel.get(index).name);
    }
}

/**
 * Gets index of a network.
 *
 * @memberof NetworkList
 * @param string network Network name.
 * @return integer Index of network list.
 */
function networkIndex (network)
{
    var item;
    var i     = 0;
    var j     = -1;
    var found = false;

    while ( i < networkListModel.count && !found )
    {
        item = networkListModel.get(i);

        if ( item.server && item.name === network )
        {
            j     = i;
            found = true;
        }
        else
        {
            i++;
        }
    }

    return j;
}

/**
 * Gets index of a chat.
 *
 * @memberof NetworkList
 * @param string network Network name.
 * @param string chat    Chat name.
 * @return integer Index of network list.
 */
function chatIndex (network, chat)
{
    var item;
    var i     = 0;
    var j     = -1;
    var found = false;

    while ( i < networkListModel.count && !found )
    {
        item = networkListModel.get(i);

        if ( !item.server && item.name === chat && item.network === network )
        {
            j     = i;
            found = true;
        }
        else
        {
            i++;
        }
    }

    return j;
}

/**
 * Checks if network has chats.
 *
 * @memberof NetworkList
 * @param string network Network name.
 * @return boolean True if network has chats.
 */
function hasChats (network)
{
    return chats.hasOwnProperty(network);
}

/**
 * Unsets selected chat.
 *
 * @memberof NetworkList
 */
function unsetSelected ()
{
    var index = -1;

    if ( selectedNetwork )
    {
        if ( selectedChat != "(server)" )
        {
            index = chatIndex(selectedNetwork, selectedChat);
        }
        else
        {
            index = networkIndex(selectedNetwork);
        }

        if ( index > -1 )
        {
            networkListModel.get(index).selected = false;
        }
    }
}

/**
 * Called when string setting has changed.
 *
 * @memberof NetworkList
 * @param string type    Setting type.
 * @param string setting Setting name.
 * @param string value   Setting value.
 */
function stringSettingChanged (type, setting, value)
{
    if ( type === cType )
    {
        switch ( setting )
        {
            case cNewMsg:
            {
                newMsgColor = value;

                break;
            }

            case cHighlight:
            {
                highlightColor = value;

                break;
            }

            case cPrivateMsg:
            {
                privateMsgColor = value;

                break;
            }
        }
    }
}
