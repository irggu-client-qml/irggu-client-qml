/****************************************************************************
**  ColorButtonStyle.qml
**
**  Copyright information
**
**      Copyright (C) 2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

import QtQuick 2.2
import QtQuick.Controls.Styles 1.2

/**
 *  Options color button style.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2014-06-08
 */
ButtonStyle
{
    id: colorButtonStyle

    /** Color category. */
    property string category;

    /** Color */
    property string color

    /** Color number. */
    property int index;

    /** type:void Function to insert the style to Options instance. */
    property var insertStyle;

    /**
     * Sets color to button.
     *
     * @param type:string newColor New color for the button.
     */
    function setColor (newColor)
    {
        color = newColor;
    }

    Component.onCompleted: insertStyle(category, index, setColor)

    background: Rectangle
    {
        implicitWidth: 25
        implicitHeight: 25
        border.width: control.activeFocus ? 2 : 1
        border.color: "#888"
        radius: 4
        gradient: Gradient {
            GradientStop { position: 0 ; color: control.pressed ? "#ccc" : colorButtonStyle.color }
            GradientStop { position: 1 ; color: control.pressed ? "#aaa" : colorButtonStyle.color }
        }
    }
}
