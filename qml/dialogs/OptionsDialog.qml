/****************************************************************************
**  OptionsDialog.qml
**
**  Copyright information
**
**      Copyright (C) 2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.1
import "../styles/"
import "../js/Options.js" as Options

/**
 *  Options dialog.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2014-06-17
 */
Window
{
    id: optionsDialog
    objectName: "optionsDialog"

    maximumWidth: 570
    maximumHeight: 260
    minimumWidth: 570
    minimumHeight: 260

    color: palette.window

    title: qsTr("Options")
    flags: Qt.Dialog
    modality: Qt.WindowModal

    /**
     * Get instance of Options.
     *
     * @return type:Options Instance of Options.
     */
    function js ()
    {
        return Options;
    }

    /**
     * Write setting to server.
     *
     * @param category Setting category.
     * @param setting  Setting name.
     * @param value    Setting value.
     */
    signal writeSetting(string category, string setting, string value)

    Component.onCompleted: Options.init()

    ColorDialog
    {
        id: colorDialog

        property string category
        property int index

        onAccepted: Options.colorDialogClosed()
    }

    FontDialog
    {
        id: fontDialog

        onAccepted: Options.fontDialogClosed()
    }

    ColumnLayout
    {
        anchors.fill: parent
        anchors.margins: 4

        TabView
        {
            id: tabView

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: closeButton.top
            anchors.bottomMargin: 4

            onCurrentIndexChanged: Options.tabSelected(currentIndex)

            ColumnLayout
            {
                id: appearanceTab

                anchors.top: parent.top
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.topMargin: 10
                anchors.leftMargin: 4

                Text
                {
                    text: qsTr("Chatbox colors:")
                    font.bold: true
                    renderType: TextInput.NativeRendering
                }

                GridLayout
                {
                    id: appearance1
                    columns: 17

                    Text
                    {
                        text: qsTr("Mirc colors:")
                        renderType: TextInput.NativeRendering
                    }

                    Repeater
                    {
                        model: 16

                        Button
                        {
                            id: mircColor

                            property string category: "mirc"
                            property int colorIndex : index

                            text: colorIndex
                            onClicked: Options.openColorDialog(category, colorIndex)
                            style: ColorButtonStyle
                            {
                                category: mircColor.category
                                index: mircColor.colorIndex
                                insertStyle: Options.insertStyle
                            }
                        }
                    }

                    Text
                    {
                        text: qsTr("Local colors:")
                        renderType: TextInput.NativeRendering
                    }

                    Repeater
                    {
                        model: 16

                        Button
                        {
                            id: localColor

                            property string category: "local"
                            property int colorIndex : index

                            text: colorIndex
                            onClicked: Options.openColorDialog(category, colorIndex)
                            style: ColorButtonStyle
                            {
                                category: localColor.category
                                index: localColor.colorIndex
                                insertStyle: Options.insertStyle
                            }
                        }
                    }
                }

                RowLayout
                {
                    id: appearance2

                    anchors.top: appearance1.bottom
                    anchors.topMargin: 10

                    Text
                    {
                        text: qsTr("Foreground:")
                        renderType: TextInput.NativeRendering
                    }

                    Button
                    {
                        id: foregroundColor

                        property string category: "interface"
                        property int colorIndex : 0

                        text: qsTr("F")
                        onClicked: Options.openColorDialog(category, colorIndex)
                        style: ColorButtonStyle
                        {
                            category: foregroundColor.category
                            index: foregroundColor.colorIndex
                            insertStyle: Options.insertStyle
                        }
                    }

                    Text
                    {
                        text: qsTr("Background:")
                        renderType: TextInput.NativeRendering
                    }

                    Button
                    {
                        id: backgroundColor

                        property string category: "interface"
                        property int colorIndex : 1

                        text: qsTr("B")
                        onClicked: Options.openColorDialog(category, colorIndex)
                        style: ColorButtonStyle
                        {
                            category: backgroundColor.category
                            index: backgroundColor.colorIndex
                            insertStyle: Options.insertStyle
                        }
                    }
                }

                RowLayout
                {
                    id: appearance3

                    anchors.top: appearance2.bottom
                    anchors.topMargin: 10

                    Text
                    {
                        text: qsTr("Chatlist colors:")
                        font.bold: true
                        renderType: TextInput.NativeRendering
                    }

                    Text
                    {
                        text: qsTr("New message:")
                        renderType: TextInput.NativeRendering
                    }

                    Button
                    {
                        id: newMsgColor

                        property string category: "interface"
                        property int colorIndex : 2

                        text: qsTr("M")
                        onClicked: Options.openColorDialog(category, colorIndex)
                        style: ColorButtonStyle
                        {
                            category: newMsgColor.category
                            index: newMsgColor.colorIndex
                            insertStyle: Options.insertStyle
                        }
                    }

                    Text
                    {
                        text: qsTr("Highlight:")
                        renderType: TextInput.NativeRendering
                    }

                    Button
                    {
                        id: highlightColor

                        property string category: "interface"
                        property int colorIndex : 3

                        text: qsTr("H")
                        onClicked: Options.openColorDialog(category, colorIndex)
                        style: ColorButtonStyle
                        {
                            category: highlightColor.category
                            index: highlightColor.colorIndex
                            insertStyle: Options.insertStyle
                        }
                    }

                    Text
                    {
                        text: qsTr("Private message:")
                        renderType: TextInput.NativeRendering
                    }

                    Button
                    {
                        id: privateMsgColor

                        property string category: "interface"
                        property int colorIndex : 4

                        text: qsTr("P")
                        onClicked: Options.openColorDialog(category, colorIndex)
                        style: ColorButtonStyle
                        {
                            category: privateMsgColor.category
                            index: privateMsgColor.colorIndex
                            insertStyle: Options.insertStyle
                        }
                    }
                }

                RowLayout
                {
                    id: appearance4

                    anchors.top: appearance3.bottom
                    anchors.topMargin: 10

                    Text
                    {
                        text: qsTr("Font:")
                        font.bold: true
                        renderType: TextInput.NativeRendering
                    }

                    Button
                    {
                        id:fontButton

                        onClicked: Options.openFontDialog()
                    }
                }
            }


            RowLayout
            {
                id: behaviourTab

                anchors.fill: parent
                anchors.topMargin: 10
                anchors.leftMargin: 4

                ColumnLayout
                {
                    id: behaviour1

                    anchors.top: parent.top
                    anchors.bottom: parent.bottom

                    Text
                    {
                        text: qsTr("Systray:")
                        font.bold: true
                        renderType: TextInput.NativeRendering
                    }

                    CheckBox
                    {
                        id: showSystrayIcon

                        text: qsTr("Show systray icon.")
                        onCheckedChanged: Options.showSystrayChanged()
                    }

                    CheckBox
                    {
                        id: minimizeToSystray

                        text: qsTr("Minimize application to systray.")
                        onCheckedChanged: Options.minimizeToSystrayChanged()
                        enabled: false
                    }

                    CheckBox
                    {
                        id: closeToSystray

                        text: qsTr("Close application to systray.")
                        onCheckedChanged: Options.closeToSystrayChanged()
                        enabled: false
                    }

                    Text
                    {
                        id: selectingLabel

                        anchors.top: closeToSystray.bottom
                        anchors.topMargin: 20
                        text: qsTr("Selecting:")
                        font.bold: true
                        renderType: TextInput.NativeRendering
                    }

                    CheckBox
                    {
                        id: selectTimestamp

                        anchors.top: selectingLabel.bottom
                        text: qsTr("Select timestamp.")
                        onCheckedChanged: Options.selectTimestampChanged()
                    }
                }

                ColumnLayout
                {
                    anchors.top: parent.top
                    anchors.left: behaviour1.right
                    anchors.leftMargin: 76

                    Text
                    {
                        text: qsTr("Alerts:")
                        font.bold: true
                        renderType: TextInput.NativeRendering
                    }

                    CheckBox
                    {
                        id: alertWindowActive

                        text: qsTr("Alert when window is active.")
                        onCheckedChanged: Options.alertWindowActiveChanged()
                    }

                    CheckBox
                    {
                        id: alertWindowDeactive

                        text: qsTr("Alert when window is deactive.")
                        onCheckedChanged: Options.alertWindowDeactiveChanged()
                    }

                    CheckBox
                    {
                        id: alertWindowClosed

                        text: qsTr("Alert when window is closed.")
                        onCheckedChanged: Options.alertWindowClosedChanged()
                    }

                    CheckBox
                    {
                        id: alertShowNotification

                        text: qsTr("Show notification.")
                        onCheckedChanged: Options.alertShowNotificationChanged()
                    }

                    CheckBox
                    {
                        id: alertBlinkSystray

                        text: qsTr("Blink systray icon.")
                        onCheckedChanged: Options.alertBlinkSystrayChanged()
                    }
                }
            }

            RowLayout
            {
                id: miscTab

                anchors.fill: parent
                anchors.topMargin: 10
                anchors.leftMargin: 4
                spacing: 0

                ColumnLayout
                {
                    id: misc1

                    anchors.top: parent.top

                    Text
                    {
                        text: qsTr("Timestamp:")
                        font.bold: true
                        renderType: TextInput.NativeRendering
                    }

                    CheckBox
                    {
                        id: timestampEnabled
                        text: qsTr("Enable timestamp.")
                        onCheckedChanged: Options.timestampEnabledChanged()
                    }

                    Rectangle
                    {
                        width: 180
                        height: timestampFormat.height + 10
                        border.width: 1
                        border.color: palette.mid
                        radius: 6

                        TextInput
                        {
                            id: timestampFormat

                            anchors.centerIn: parent
                            width: parent.width - font.pixelSize
                            font.pixelSize: 12
                            renderType: TextInput.NativeRendering
                            selectByMouse: true
                            clip: true
                        }
                    }
                }

                ColumnLayout
                {
                    id: misc2

                    anchors.top: parent.top
                    anchors.left: misc1.right
                    anchors.leftMargin: 106

                    Text
                    {
                        text: qsTr("Priority:")
                        font.bold: true
                        renderType: TextInput.NativeRendering
                    }

                    ComboBox
                    {
                        id: priority

                        model: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                        onCurrentIndexChanged: Options.priorityChanged(currentIndex)
                    }

                    Text
                    {
                        id: scrollbackLabel

                        anchors.top: priority.bottom
                        anchors.topMargin: 20
                        text: qsTr("Scrollback:")
                        font.bold: true
                        renderType: TextInput.NativeRendering
                    }

                    Rectangle
                    {
                        anchors.top: scrollbackLabel.bottom
                        anchors.left: parent.left
                        anchors.right: parent.right
                        height: scrollback.height + 10
                        border.width: 1
                        border.color: palette.mid
                        radius: 6

                        TextInput
                        {
                            id: scrollback

                            anchors.centerIn: parent
                            width: parent.width - font.pixelSize
                            font.pixelSize: 12
                            renderType: TextInput.NativeRendering
                            selectByMouse: true
                            clip: true
                        }
                    }
                }
            }
        }

        Button
        {
            id: closeButton

            anchors.right: parent.right
            text: qsTr("Close")
            onClicked: Options.close();
        }
    }
}
