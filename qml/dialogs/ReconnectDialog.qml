/****************************************************************************
**  ReconnectDialog.qml
**
**  Copyright information
**
**      Copyright (C) 2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import "../js/Reconnect.js" as Reconnect

/**
 *  Reconnect dialog.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2014-11-16
 */
Window
{
    objectName: "reconnectDialog"

    maximumWidth: 230
    maximumHeight: 50
    minimumWidth: 230
    minimumHeight: 50

    color: palette.window

    title: qsTr("Reconnect")
    flags: Qt.Dialog
    modality: Qt.WindowModal

    /**
     * Get instance of Reconnect.
     *
     * @return type:Reconnect Instance of Reconnect.
     */
    function js ()
    {
        return Reconnect;
    }

    /**
     * Reconnect to server.
     */
    signal reconnect()

    /**
     * Client has logged into server.
     */
    signal logged()

    /**
     * Cancel reconnecting.
     */
    signal cancelReconnecting()

    Component.onCompleted: Reconnect.init()
    onVisibleChanged: Reconnect.visibleChanged(arg)

    Timer
    {
        id: messageTimer

        interval: 1000
        repeat: true
        triggeredOnStart: true
        onTriggered: Reconnect.updateCountdown()
    }

    ColumnLayout
    {
        anchors.fill: parent
        anchors.margins: 4

        Text
        {
            id: messageText

            renderType: TextInput.NativeRendering
        }

        Button
        {
            id: closeButton

            anchors.right: parent.right
            text: qsTr("Cancel")
            onClicked: Reconnect.cancel();
        }
    }
}
