/****************************************************************************
**  DccDialog.qml
**
**  Copyright information
**
**      Copyright (C) 2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import "../js/Dcc.js" as Dcc

/**
 *  DCC dialog.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2014-07-05
 */
Window
{
    id: dccDialog
    objectName: "dccDialog"

    maximumWidth: 510
    maximumHeight: 250
    minimumWidth: 510
    minimumHeight: 250

    color: palette.window

    title: qsTr("File Transfers")
    flags: Qt.Dialog
    modality: Qt.WindowModal

    /**
     * Get instance of Dcc.
     *
     * @return type:Dcc Instance of Dcc.
     */
    function js ()
    {
        return Dcc;
    }

    /**
     * Write DCC file reply.
     *
     * @param network Network name.
     * @param sender  Sender's name.
     * @param file    Filename.
     * @param get     Specifies if the file should be accepted.
     */
    signal writeDccFileReply(string network, string sender, string file, bool get)

    ColumnLayout
    {
        anchors.fill: parent
        anchors.margins: 4

        TableView
        {
            id: dccTableView

            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: buttons.top
            anchors.bottomMargin: 4

            onClicked: Dcc.clicked(row)

            model: ListModel
            {
                id: dccModel
            }

            TableViewColumn
            {
                role: "network"
                title: qsTr("Network")
                width: 120
                movable: false
                resizable: false
            }

            TableViewColumn
            {
                role: "sender"
                title: qsTr("Sender")
                width: 120
                movable: false
                resizable: false
            }

            TableViewColumn
            {
                role: "file"
                title: qsTr("File")
                width: 260
                movable: false
                resizable: false
            }
        }

        RowLayout
        {
            id: buttons
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom

            Button
            {
                id: acceptButton

                anchors.left: parent.left
                text: qsTr("Accept")
                onClicked: Dcc.accept()
                enabled: false
            }

            Button
            {
                id: discardButton

                anchors.left: acceptButton.right
                anchors.leftMargin: 4
                text: qsTr("Discard")
                onClicked: Dcc.discard()
                enabled: false
            }

            Button
            {
                id: closeButton

                anchors.right: parent.right
                text: qsTr("Close")
                onClicked: dccDialog.close()
            }
        }
    }
}
