/****************************************************************************
**  LoginDialog.qml
**
**  Copyright information
**
**      Copyright (C) 2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import "../js/Login.js" as Login

/**
 *  Login dialog.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2014-11-15
 */
Window
{
    objectName: "loginDialog"

    maximumWidth: 250
    maximumHeight: 250
    minimumWidth: 250
    minimumHeight: 250

    color: palette.window

    flags: Qt.Dialog
    modality: Qt.WindowModal

    /**
     * Get instance of Login.
     *
     * @return type:Login Instance of Login.
     */
    function js ()
    {
        return Login;
    }

    /**
     * Login to server.
     *
     * @param server          Server.
     * @param username        Username.
     * @param password        Password.
     * @param useSsl          Specifies if SSL is used.
     * @param ignoreSslErrors Specifies if SSL errors are ignored.
     * @param temporary       Specifies if session is temporary.
     */
    signal login(string server, string username, string password, bool useSsl,
                 bool ignoreSslErrors, bool temporary)
    /**
     * Client has logged into server.
     */
    signal logged()

    /**
     * Logout from server.
     */
    signal logout()

    Component.onCompleted: Login.init()
    onVisibleChanged: Login.visibleChanged(arg)

    ColumnLayout
    {
        anchors.fill: parent
        anchors.margins: 4

        GridLayout
        {
            anchors.left: parent.left
            anchors.right: parent.right

            columns: 2

            Text
            {
                id: serverLabel
                text: qsTr("Server:")
                font.pixelSize: 12
                renderType: Text.NativeRendering
            }

            Rectangle
            {
                anchors.left: usernameLabel.right
                anchors.right: parent.right
                anchors.leftMargin: 4
                height: server.height + 10
                border.width: 1
                border.color: palette.mid
                radius: 6

                TextInput
                {
                    id: server

                    anchors.centerIn: parent
                    width: parent.width - font.pixelSize
                    font.pixelSize: 12
                    renderType: TextInput.NativeRendering
                    selectByMouse: true
                    clip: true
                }
            }

            Text
            {
                id: usernameLabel
                text: qsTr("Username:")
                font.pixelSize: 12
                renderType: Text.NativeRendering
            }

            Rectangle
            {
                anchors.left: usernameLabel.right
                anchors.right: parent.right
                anchors.leftMargin: 4
                height: username.height + 10
                border.width: 1
                border.color: palette.mid
                radius: 6

                TextInput
                {
                    id: username

                    anchors.centerIn: parent
                    width: parent.width - font.pixelSize
                    font.pixelSize: 12
                    renderType: TextInput.NativeRendering
                    selectByMouse: true
                    clip: true
                }
            }

            Text
            {
                id: passwordLabel
                text: qsTr("Password:")
                font.pixelSize: 12
                renderType: Text.NativeRendering
            }

            Rectangle
            {
                anchors.left: usernameLabel.right
                anchors.right: parent.right
                anchors.leftMargin: 4
                height: password.height + 10
                border.width: 1
                border.color: palette.mid
                radius: 6

                TextInput
                {
                    id: password

                    anchors.centerIn: parent
                    width: parent.width - font.pixelSize
                    font.pixelSize: 12
                    renderType: TextInput.NativeRendering
                    clip: true
                    selectByMouse: true
                    echoMode: TextInput.Password
                }
            }
        }

        CheckBox
        {
            id: useSsl
            text: qsTr("Use SSL.")
            onCheckedChanged: Login.useSslChanged()
        }

        CheckBox
        {
            id: ignoreSslErrors
            text: qsTr("Ignore SSL errors.")
            enabled: false
        }

        CheckBox
        {
            id: rememberMe
            text: qsTr("Remember me.")
            onCheckedChanged: Login.rememberMeChanged()
        }

        CheckBox
        {
            id: rememberPassword
            text: qsTr("Remember password.")
            enabled: false
            onCheckedChanged: Login.rememberPasswordChanged()
        }

        CheckBox
        {
            id: loginOnStartup
            text: qsTr("Login on startup.")
            enabled: false
        }

        RowLayout
        {
            Button
            {
                id: loginButton
                text: qsTr("Login")
                onClicked: Login.startLogin()
            }

            Button
            {
                id: closeButton
                text: qsTr("Close")
                onClicked: Qt.quit()
            }
        }
    }
}
