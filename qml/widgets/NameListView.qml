/****************************************************************************
**  NameListView.qml
**
**  Copyright information
**
**      Copyright (C) 2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

import QtQuick 2.1
import "../js/NameList.js" as NameList

/**
 *  ListView for name list.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2014-06-17
 */
Rectangle
{
    objectName: "nameList"

    color: palette.window

    /**
     * Get instance of NameList.
     *
     * @return type:NameList Instance of NameList.
     */
    function js ()
    {
        return NameList;
    }

    /**
     * Opens private chat with user.
     *
     * @param network Network name.
     * @param nick    User's nick.
     */
    signal query(string network, string nick)

    Rectangle
    {
        anchors.fill: parent
        anchors.topMargin: 4
        anchors.rightMargin: 4
        anchors.bottomMargin: 4
        border.width: 1
        border.color: palette.mid
        radius: 4

        ListView
        {
            id: nameListView
            anchors.fill: parent

            delegate: Item
            {
                width: parent.width
                height: nick.paintedHeight + 10

                Rectangle
                {
                    id: nickBg
                    anchors.fill: parent
                    radius: 4
                    color: palette.highlight
                    opacity: hover ? 0.4 : 0

                    MouseArea
                    {
                        anchors.fill: parent
                        hoverEnabled: true
                        onEntered: NameList.mouseOn(index)
                        onExited: NameList.mouseOff(index)
                        onClicked: NameList.clicked(index)
                    }
                }

                Text
                {
                    id: nick
                    text: name
                    anchors
                    {
                        left: parent.left
                        right: parent.right
                        verticalCenter: parent.verticalCenter
                        leftMargin: 4
                    }
                    renderType: Text.NativeRendering
                }
            }

        }
    }
}
