/****************************************************************************
**  ChatBoxView.qml
**
**  Copyright information
**
**      Copyright (C) 2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

import QtQuick 2.2
import "../js/ChatBox.js" as ChatBox
import "../js/ChatBoxSelection.js" as ChatBoxSelection

/**
 *  Chat box.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2014-06-17
 */
Rectangle
{
    objectName: "chatBox"

    color: palette.window

    /**
     * Get instance of ChatBox.
     *
     * @return type:ChatBox Instance of ChatBox.
     */
    function js ()
    {
        return ChatBox;
    }

    /**
     * Sets ChatBoxListView for ChatBoxSelection.
     *
     * @param type:ChatBoxListView view Instance of ChatBoxListView.
     */
    function setView (view)
    {
        ChatBoxSelection.setView(view);
    }

    /**
     * Write to chat.
     *
     * @param network Network name.
     * @param chat    Chat name.
     * @param line    Line to be written.
     */
    signal write(string network, string chat, string line)

    /**
     * Complete line.
     *
     * @param network Network name.
     * @param chat    Chat name.
     * @param line    Line to be completed.
     * @param pos     Cursor position.
     */
    signal complete(string network, string chat, string line, int pos)

    Rectangle
    {
        id: chatBoxListViewContainer
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: chatBoxLineBorder.top
        anchors.margins: 4
        border.width: 1
        border.color: palette.mid
        radius: 4
    }

    MouseArea
    {
        id: chatBoxMouseArea
        anchors.fill: chatBoxListViewContainer
        acceptedButtons: Qt.LeftButton
        hoverEnabled: true
        onClicked: ChatBoxSelection.mouseClicked(mouse)
        onPressed: ChatBoxSelection.mousePressed(mouse)
        onPositionChanged: ChatBoxSelection.mouseMove(mouse)
        onReleased: ChatBoxSelection.mouseReleased(mouse)
    }

    Rectangle
    {
        id: chatBoxLineBorder

        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 4
        height: chatBoxLineView.height + 10
        border.width: 1
        border.color: palette.mid
        radius: 4

        TextInput
        {
            id: chatBoxLineView
            anchors.centerIn: parent
            width: parent.width - font.pixelSize
            font.pixelSize: 12
            clip: true
            focus: true
            renderType: TextEdit.NativeRendering
            Keys.onUpPressed: ChatBox.lineBufferUp()
            Keys.onDownPressed: ChatBox.lineBufferDown()
            Keys.onReturnPressed: ChatBox.writeLine()
            Keys.onTabPressed: ChatBox.completeLine()
        }
    }

    Connections
    {
        target: settings
        onBoolSettingChanged: ChatBoxSelection.boolSettingChanged(type, setting, value)
        onIntSettingChanged: ChatBox.intSettingChanged(type, setting, value)
        onStringSettingChanged: ChatBox.stringSettingChanged(type, setting, value)
        onFontSettingChanged: ChatBox.fontSettingChanged(type, setting, value)
    }
}
