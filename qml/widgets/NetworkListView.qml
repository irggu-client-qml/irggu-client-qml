/****************************************************************************
**  NetworkListView.qml
**
**  Copyright information
**
**      Copyright (C) 2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

import QtQuick 2.1
import QtQuick.Controls 1.2
import "../js/NetworkList.js" as NetworkList

/**
 *  ListView for network list.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2014-06-17
 */
Rectangle
{
    objectName: "networkList"

    color: palette.window

    Component.onCompleted: NetworkList.init()

    /**
     * Get instance of NetworkList.
     *
     * @return type:NetworkList Instance of NetworkList.
     */
    function js ()
    {
        return NetworkList;
    }

    /**
     * Chat selected.
     *
     * @param network Network name.
     * @param chat    Chat name.
     */
    signal chatSelected(string network, string chat)

    /**
     * Quits network.
     *
     * @param network Network name.
     */
    signal quitNetwork(string network)

    /**
     * Closes chat.
     *
     * @param network Network name.
     * @param chat    Chat name.
     */
    signal closeChat(string network, string chat)

    Menu
    {
        id: networkListMenu

        property int index;

        MenuItem
        {
            text: qsTr("Close")
            onTriggered: NetworkList.close(networkListMenu.index);
        }
    }

    Rectangle
    {
        anchors.fill: parent
        anchors.topMargin: 4
        anchors.leftMargin: 4
        anchors.bottomMargin: 4
        border.width: 1
        border.color: palette.mid
        radius: 4

        ListView
        {
            id: networkListView
            anchors.fill: parent
            clip: true

            model: ListModel
            {
                id: networkListModel
            }


            delegate: Item
            {
                width: parent.width
                height: channelName.paintedHeight + 10

                Rectangle
                {
                    id: toggleChats
                    width: server ? 20 : 30
                    height: parent.height
                    anchors
                    {
                        left: parent.left
                    }
                    color: "transparent"

                    Image
                    {
                        source: "qrc:///icons/arrow_" + ( chats ? "down" : "right" ) + ".svg"
                        anchors
                        {
                            left: parent.left
                            right: parent.right
                            verticalCenter: parent.verticalCenter
                        }
                        fillMode: Image.PreserveAspectFit
                        visible: server
                    }

                    MouseArea
                    {
                        anchors.fill: parent
                        onClicked: if ( server )
                        {
                            NetworkList.toggleChats(index);
                        }
                    }
                }

                Rectangle
                {
                    id: channelNameBg
                    anchors
                    {
                        top: parent.top
                        bottom: parent.bottom
                        left: toggleChats.right
                        right: parent.right
                    }
                    radius: 4
                    color: palette.highlight
                    opacity: selected && hover ? 0.6 : selected ? 0.8 : hover ? 0.4 : 0

                    MouseArea
                    {
                        anchors.fill: parent
                        hoverEnabled: true
                        acceptedButtons: Qt.AllButtons;
                        onEntered: NetworkList.mouseOn(index)
                        onExited: NetworkList.mouseOff(index)
                        onClicked: NetworkList.clicked(mouse, index)
                    }
                }

                Text
                {
                    id: channelName
                    text: name
                    anchors
                    {
                        left: toggleChats.right
                        right: parent.right
                        verticalCenter: parent.verticalCenter
                    }
                    color: colorCode
                    renderType: Text.NativeRendering
                }
            }
        }
    }
}
