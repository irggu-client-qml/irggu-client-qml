/****************************************************************************
**  ChatBoxListView.qml
**
**  Copyright information
**
**      Copyright (C) 2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

import QtQuick 2.0

/**
 *  ListView for chat box.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2014-06-15
 */
ListView
{
    id: chatBoxListView
    anchors.fill: parent
    anchors.leftMargin: 4
    anchors.rightMargin: 4
    anchors.topMargin: 2
    anchors.bottomMargin: 2
    clip: true

    /** Chat box font */
    property font font;

    delegate: Item
    {
        anchors.left: parent.left
        anchors.right: parent.right
        height: msgTextEdit.paintedHeight

        TextEdit
        {
            id: timestampTextEdit
            objectName: "timestamp"

            text: timestamp
            anchors.left: parent.left
            textFormat: TextEdit.RichText
            readOnly: true
            font: chatBoxListView.font
            renderType: TextEdit.NativeRendering
        }

        TextEdit
        {
            id: whitespaceTextEdit1
            objectName: "whitespace1"

            width: timestamp.length ? 4 : 0
            anchors.left: timestampTextEdit.right
            textFormat: TextEdit.RichText
            readOnly: true
            font: chatBoxListView.font
            renderType: TextEdit.NativeRendering
        }

        TextEdit
        {
            id: nickTextEdit
            objectName: "nick"

            text: nick
            anchors.left: whitespaceTextEdit1.right
            textFormat: TextEdit.RichText
            readOnly: true
            font: chatBoxListView.font
            renderType: TextEdit.NativeRendering
        }

        TextEdit
        {
            id: whitespaceTextEdit2
            objectName: "whitespace2"

            width: 4
            anchors.left: nickTextEdit.right
            textFormat: TextEdit.RichText
            readOnly: true
            font: chatBoxListView.font
            renderType: TextEdit.NativeRendering
        }

        TextEdit
        {
            id: msgTextEdit
            objectName: "msg"

            text: msg
            anchors.left: whitespaceTextEdit2.right
            anchors.right: parent.right
            textFormat: TextEdit.RichText
            wrapMode: TextEdit.Wrap
            readOnly: true
            font: chatBoxListView.font
            renderType: TextEdit.NativeRendering
        }
    }
}
