/****************************************************************************
**  MainWindow.qml
**
**  Copyright information
**
**      Copyright (C) 2013-2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-QML.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

import QtQuick 2.1
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import "dialogs"
import "widgets"
import "js/IrGGu.js" as IrGGu

/**
 *  Main window.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2014-07-05
 */
ApplicationWindow
{
    id: mainWindow
    title: qsTr("IrGGu")
    onActiveChanged: IrGGu.activeChanged()
    onVisibleChanged: IrGGu.visibleChanged(arg)

    SystemPalette { id: palette; colorGroup: SystemPalette.Active }

    color: palette.window

    /**
     * Signals that GUI is ready for data.
     *
     * @param newOnly Only send new data.
     */
    signal readyForData(bool newOnly)

    /**
     * Write set idle command to server.
     *
     * @param idle     Specifies if client is in idle state.
     * @param alerts   Specifies if client shows alerts.
     * @param priority Priority of client.
     */
    signal setIdle(bool idle, bool alerts, int priority)

    Component.onCompleted: IrGGu.init()

    QtObject
    {
        id: array

        function sort (a, b)
        {
            var aLower = a.toLowerCase();
            var bLower = b.toLowerCase();

            return aLower < bLower ? -1 : bLower < aLower ? 1 : 0;
        }
    }

    QtObject
    {
        id: json

        function size (json)
        {
            var size = 0;

            for ( var property in json )
            {
                size++;
            }

            return size;
        }
    }

    QtObject
    {
        id: string

        function isEmpty (string)
        {
            return string.length === 0 || /^\s*$/.test(string);
        }
    }

    menuBar: MenuBar
    {

        Menu
        {
            title: qsTr("IrGGu")

            MenuItem
            {
                text: qsTr("Options")
                onTriggered: optionsDialog.show()
            }

            MenuItem
            {
                text: qsTr("File Transfers")
                onTriggered: dccDialog.show()
            }

            MenuItem
            {
                text: qsTr("Logout")
                onTriggered: IrGGu.logout()
            }

            MenuItem
            {
                text: qsTr("Quit")
                onTriggered: IrGGu.quit();
            }
        }
    }

    LoginDialog
    {
        id: loginDialog
        onLogged: IrGGu.logged()
    }

    OptionsDialog
    {
        id: optionsDialog
    }

    DccDialog
    {
        id: dccDialog
    }

    ReconnectDialog
    {
        id: reconnectDialog
        onLogged: IrGGu.logged()
        onCancelReconnecting: IrGGu.logout()
    }

    SplitView
    {
        anchors.fill: parent
        orientation: Qt.Horizontal

        handleDelegate: Rectangle
        {
            width: 0
            height: 0
        }

        NetworkListView
        {
            id: networkListView
            width: 200
        }

        ChatBoxView
        {
            id: chatBoxView
            Layout.minimumWidth: 50
            Layout.fillWidth: true
        }

        NameListView
        {
            id: nameListView
            width: 0
            visible: false
        }
    }

    Connections
    {
        target: client
        onScreensaverStateChanged: IrGGu.screensaverStateChanged(active)
    }

    Connections
    {
        target: session
        onReady: IrGGu.sessionReady()
        onConnected: IrGGu.connected()
        onUnableToConnect: IrGGu.unableToConnect()
        onDisconnected: IrGGu.disconnected()
        onLoginResult: IrGGu.loginResult(result)
        onInsertNetwork: IrGGu.insertNetwork(network)
        onInsertChat: IrGGu.insertChat(network, chat)
        onInsertNick: IrGGu.insertNick(network, chat, nick, index)
        onInsertNicks: IrGGu.insertNicks(network, chat, nicks)
        onRemoveNetwork: IrGGu.removeNetwork(network)
        onRemoveChat: IrGGu.removeChat(network, chat)
        onRemoveNick: IrGGu.removeNick(network, chat, index)
        onRenameChat: IrGGu.renameChat(network, oldChat, newChat)
        onNewMsg: IrGGu.newMsg(network, chat, timestamp, nick, msg, type)
        onNewDCCFile: IrGGu.newDCCFile(network, sender, file)
        onCloseDCCFile: IrGGu.closeDCCFile(network, sender, file)
        onLineCompleted: IrGGu.lineCompleted(network, chat, line)
        onShowAlert: IrGGu.showAlert(network, sender, type)
        onCloseAlert: IrGGu.closeAlert(network, sender, type)
    }

    Connections
    {
        target: systray
        onClicked: IrGGu.systrayClicked()
        onMenuClicked: IrGGu.systrayMenuClicked(action)
    }

    Connections
    {
        target: networkListView
        onChatSelected: IrGGu.chatSelected(network, chat)
    }
}
